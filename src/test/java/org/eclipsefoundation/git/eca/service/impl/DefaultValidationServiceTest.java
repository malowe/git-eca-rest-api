package org.eclipsefoundation.git.eca.service.impl;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatus;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.GitUser;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.model.ValidationResponse;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.eclipsefoundation.git.eca.service.ValidationService;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

@QuarkusTest
class DefaultValidationServiceTest {
    private static final String VALID_ECA_MAIL = "slom@eclipse-foundation.org";
    private static final String INVALID_ECA_MAIL = "newbie@important.co";

    @Inject
    ValidationService svc;

    @Inject
    PersistenceDao dao;
    @Inject
    FilterService filters;

    /**
     * Addresses issue #160. While this could have been done to just update the return of data, we should also actively clean out bad
     * messages as they are resolved on validation as well.
     */
    @Test
    void updateCommitValidationStatus_removesDuplicateMessagesOnValidation() {
        String errorCommitHash = "duplicate-message-err-02";

        RequestWrapper wrap = new FlatRequestWrapper(URI.create("https://api.eclipse.org/git/eca/test"));
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(GitEcaParameterNames.SHAS_RAW, errorCommitHash);
        // check that there are multiple messages on the commit
        List<CommitValidationStatus> statuses = dao.get(new RDBMSQuery<>(wrap, filters.get(CommitValidationStatus.class), params));
        CommitValidationStatus status = statuses.get(0);
        Assertions.assertTrue(status.getErrors().size() > 1);

        ValidationResponse out = svc
                .validateIncomingRequest(ValidationRequest
                        .builder()
                        .setCommits(Arrays
                                .asList(getCommit("duplicate-message-err-01", VALID_ECA_MAIL, VALID_ECA_MAIL),
                                        getCommit(errorCommitHash, INVALID_ECA_MAIL, VALID_ECA_MAIL)))
                        .setEstimatedLoc(0)
                        .setProvider(ProviderType.GITHUB)
                        .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/test"))
                        .setStrictMode(true)
                        .build(), wrap);

        // should have 1 error, with a certain commit as the source
        Assertions.assertFalse(out.getPassed());
        Assertions.assertEquals(1, out.getErrorCount());
        // should update the data as well to be only 1
        statuses = dao.get(new RDBMSQuery<>(wrap, filters.get(CommitValidationStatus.class), params));
        status = statuses.get(0);
        Assertions.assertEquals(1, status.getErrors().size());
    }

    Commit getCommit(String hash, String authorMail, String committerMail) {
        return Commit
                .builder()
                .setAuthor(GitUser.builder().setMail(authorMail).setName(authorMail).build())
                .setCommitter(GitUser.builder().setMail(committerMail).setName(committerMail).build())
                .setBody("")
                .setHash(hash)
                .setHead(false)
                .setLastModificationDate(DateTimeHelper.now())
                .setParents(Collections.emptyList())
                .setSubject("")
                .build();
    }
}
