/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatus;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.GitUser;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.eclipsefoundation.git.eca.service.ValidationStatusService;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

@QuarkusTest
class DefaultValidationStatusServiceTest {

    @Inject
    ValidationStatusService validationStatus;

    @Inject
    PersistenceDao dao;
    @Inject
    FilterService filters;

    @Test
    void getHistoricValidationStatus_noFingerprint() {
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        Assertions.assertTrue(validationStatus.getHistoricValidationStatus(wrap, null).isEmpty());
        Assertions.assertTrue(validationStatus.getHistoricValidationStatus(wrap, " ").isEmpty());
        Assertions.assertTrue(validationStatus.getHistoricValidationStatus(wrap, "").isEmpty());
    }

    @Test
    void getHistoricValidationStatus_noResultsForFingerprint() {
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        Assertions.assertTrue(validationStatus.getHistoricValidationStatus(wrap, UUID.randomUUID().toString()).isEmpty());
    }

    @Test
    void getHistoricValidationStatus_success() {
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        Assertions.assertTrue(!validationStatus.getHistoricValidationStatus(wrap, "957706b0f31e0ccfc5287c0ebc62dc79").isEmpty());
    }

    @Test
    void getHistoricValidationStatus_success_deduplicatesMessages() {
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        List<CommitValidationStatus> results = validationStatus.getHistoricValidationStatus(wrap, "deduplicate-message-test-fingerprint");
        Assertions.assertFalse(results.isEmpty());
        Assertions.assertEquals(1, results.get(0).getErrors().size());

        // check the raw dao against results
        String errorCommitHash = "deduplicate-message-test-01";
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(GitEcaParameterNames.SHAS_RAW, errorCommitHash);
        // check that there are multiple messages on the commit
        List<CommitValidationStatus> statuses = dao.get(new RDBMSQuery<>(wrap, filters.get(CommitValidationStatus.class), params));
        Assertions.assertEquals(statuses.get(0).getCommitHash(), results.get(0).getCommitHash());
        Assertions.assertNotEquals(statuses.get(0).getErrors().size(), results.get(0).getErrors().size());
    }

    @Test
    void getRequestCommitValidationStatus_noneExisting() {
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        ValidationRequest vr = generateBaseRequest();
        List<CommitValidationStatus> out = validationStatus.getRequestCommitValidationStatus(wrap, vr, "sample.proj");
        // should always return non-null, should be empty w/ no results as there
        // shouldn't be a matching status
        Assertions.assertTrue(out.isEmpty());
    }

    @Test
    void getRequestCommitValidationStatus_existing() {
        // create request that lines up with one of the existing test commit validation statuses
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit
                .builder()
                .setAuthor(g1)
                .setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>")
                .setHash("123456789")
                .setSubject("All of the things")
                .setParents(Collections.emptyList())
                .build();
        commits.add(c1);
        ValidationRequest vr = ValidationRequest
                .builder()
                .setStrictMode(false)
                .setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample"))
                .setCommits(commits)
                .build();
        List<CommitValidationStatus> out = validationStatus.getRequestCommitValidationStatus(wrap, vr, "sample.proj");
        // should contain one of the test status objects
        Assertions.assertTrue(!out.isEmpty());
    }

    @Test
    void getRequestCommitValidationStatus_deduplicatesMessages() {
        // create request that lines up with one of the existing test commit validation statuses
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit
                .builder()
                .setAuthor(g1)
                .setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>")
                .setHash("deduplicate-message-test-01")
                .setSubject("All of the things")
                .setParents(Collections.emptyList())
                .build();
        commits.add(c1);
        ValidationRequest vr = ValidationRequest
                .builder()
                .setStrictMode(false)
                .setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/test"))
                .setCommits(commits)
                .build();
        List<CommitValidationStatus> out = validationStatus.getRequestCommitValidationStatus(wrap, vr, "sample.proj");
        // should contain one of the test status objects
        Assertions.assertFalse(out.isEmpty());

        // check the raw dao against results
        String errorCommitHash = "deduplicate-message-test-01";
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(GitEcaParameterNames.SHAS_RAW, errorCommitHash);
        // check that there are multiple messages on the commit
        List<CommitValidationStatus> statuses = dao.get(new RDBMSQuery<>(wrap, filters.get(CommitValidationStatus.class), params));

        Assertions.assertEquals(statuses.get(0).getCommitHash(), out.get(0).getCommitHash());
        Assertions.assertNotEquals(statuses.get(0).getErrors().size(), out.get(0).getErrors().size());
    }

    @Test
    void getRequestCommitValidationStatus_noProjectWithResults() {
        // create request that lines up with one of the existing test commit validation statuses
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit
                .builder()
                .setAuthor(g1)
                .setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>")
                .setHash("abc123def456")
                .setSubject("All of the things")
                .setParents(Collections.emptyList())
                .build();
        commits.add(c1);
        ValidationRequest vr = ValidationRequest
                .builder()
                .setStrictMode(false)
                .setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample"))
                .setCommits(commits)
                .build();
        List<CommitValidationStatus> out = validationStatus.getRequestCommitValidationStatus(wrap, vr, null);
        // should contain one of the test status objects
        Assertions.assertTrue(!out.isEmpty());
    }

    @Test
    void getRequestCommitValidationStatus_noProjectWithNoResults() {
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://localhost/git/eca"));
        ValidationRequest vr = generateBaseRequest();
        List<CommitValidationStatus> out = validationStatus.getRequestCommitValidationStatus(wrap, vr, null);
        // should contain one of the test status objects
        Assertions.assertTrue(out.isEmpty());
    }

    /**
     * Used when a random validationRequest is needed and will not need to be recreated/modified. Base request should register as a commit
     * for the test `sample.proj` project.
     * 
     * @return random basic validation request.
     */
    private ValidationRequest generateBaseRequest() {
        GitUser g1 = GitUser.builder().setName("The Wizard").setMail("code.wiz@important.co").build();
        List<Commit> commits = new ArrayList<>();
        // create sample commits
        Commit c1 = Commit
                .builder()
                .setAuthor(g1)
                .setCommitter(g1)
                .setBody("Signed-off-by: The Wizard <code.wiz@important.co>")
                .setHash(UUID.randomUUID().toString())
                .setSubject("All of the things")
                .setParents(Collections.emptyList())
                .build();
        commits.add(c1);
        return ValidationRequest
                .builder()
                .setStrictMode(false)
                .setProvider(ProviderType.GITHUB)
                .setRepoUrl(URI.create("http://www.github.com/eclipsefdn/sample"))
                .setCommits(commits)
                .build();
    }
}
