/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.git.eca.api.models.EclipseUser;
import org.eclipsefoundation.git.eca.helper.ProjectHelper;
import org.eclipsefoundation.git.eca.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Tests user service impl using test stub data available from API stubs. While not a perfect test as there is no auth,
 * it is good enough for unit testing.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class CachedUserServiceTest {

    @Inject
    UserService users;
    @Inject
    ProjectHelper projects;

    @Test
    void getUser_success() {
        EclipseUser u = users.getUser("grunt@important.co");
        // assert that this is the user we expect and that it exists
        Assertions.assertNotNull(u);
        Assertions.assertEquals("grunter", u.getName());
    }

    @Test
    void getUser_noReplyGH_success() {
        EclipseUser u = users.getUser("123456789+grunter2@users.noreply.github.com");
        // assert that this is the user we expect and that it exists
        Assertions.assertNotNull(u);
        Assertions.assertEquals("grunter", u.getName());
    }

    @Test
    void getUser_noReplyGH_noUser() {
        Assertions.assertNull(users.getUser("123456789+farquad@users.noreply.github.com"));
    }

    @Test
    void getUser_doesNotExist() {
        Assertions.assertNull(users.getUser("nota.realboy@pinnochio.co"));
    }

    @Test
    void getUser_nullOrEmptyEmail() {
        Assertions.assertNull(users.getUser(null));
        Assertions.assertNull(users.getUser(""));
    }

    @Test
    void getUserByGithubUsername_success() {
        EclipseUser u = users.getUserByGithubUsername("grunter2");
        // assert that this is the user we expect and that it exists
        Assertions.assertNotNull(u);
        Assertions.assertEquals("grunt@important.co", u.getMail());
    }

    @Test
    void getUserByGithubUsername_doesNotExist() {
        Assertions.assertNull(users.getUserByGithubUsername("nota.realboy"));
    }

    @Test
    void getUserByGithubUsername_nullOrEmptyUName() {
        Assertions.assertNull(users.getUserByGithubUsername(null));
        Assertions.assertNull(users.getUserByGithubUsername(""));
    }

    @Test
    void isUserABot_success() {
        Assertions.assertTrue(users.userIsABot("1.bot@eclipse.org", getTestProject()));
    }

    @Test
    void isUserABot_success_noreply() {
        Assertions.assertTrue(users.userIsABot("123456789+projbot@noreply.github.com", getTestProject()));
    }

    @Test
    void isUserABot_nullOrEmptyAddress() {
        Assertions.assertFalse(users.userIsABot(null, getTestProject()));
        Assertions.assertFalse(users.userIsABot("", getTestProject()));
    }

    @Test
    void isUserABot_nullOrEmptyProjects_botAddress() {
        Assertions.assertTrue(users.userIsABot("2.bot@eclipse.org", null));
        Assertions.assertTrue(users.userIsABot("2.bot@eclipse.org", Collections.emptyList()));
    }

    @Test
    void isUserABot_nullOrEmptyProjects_otherAddress() {
        Assertions.assertFalse(users.userIsABot("grunt@important.co", null));
        Assertions.assertFalse(users.userIsABot("grunt@important.co", Collections.emptyList()));
    }

    @Test
    void isUserABot_doesNotMatchOtherProjects() {
        Assertions.assertFalse(users.userIsABot("2.bot@eclipse.org", getTestProject()));
    }

    @Test
    void isUserABot_noMatch() {
        Assertions.assertFalse(users.userIsABot("grunt@important.co", getTestProject()));
    }

    /**
     * Gets the sample.proj project to test for project connections when needed.
     * 
     * @return the sample.proj project in a list for use in tests.
     */
    private List<Project> getTestProject() {
        Optional<Project> proj = projects.getProjects().stream().filter(p -> p.getProjectId().equals("sample.proj")).findFirst();
        if (proj.isEmpty()) {
            Assertions.fail("Could not find one of the needed test projects for test, bad state.");
        }
        return Arrays.asList(proj.get());
    }
}
