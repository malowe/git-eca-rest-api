/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.resource;

import java.util.Collections;
import java.util.Map;

import org.eclipsefoundation.git.eca.test.namespaces.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

@QuarkusTest
class StatusResourceTest {
    public static final String STATUS_BASE_URL = "/eca/status";
    public static final String FINGERPRINT_STATUS_BASE_URL = STATUS_BASE_URL + "/{fingerprint}";
    public static final String FINGERPRINT_STATUS_UI_BASE_URL = FINGERPRINT_STATUS_BASE_URL + "/ui";

    private static final String VALID_FINGERPRINT = "957706b0f31e0ccfc5287c0ebc62dc79";
    private static final String INVALID_FINGERPRINT = "nope";

    public static final EndpointTestCase GET_STATUS_SUCCESS_CASE = TestCaseHelper
            .buildSuccessCase(FINGERPRINT_STATUS_BASE_URL, new String[] { VALID_FINGERPRINT },
                    SchemaNamespaceHelper.COMMIT_VALIDATION_STATUSES_SCHEMA);

    public static final EndpointTestCase GET_STATUS_UI_NOT_FOUND_CASE = TestCaseHelper
            .prepareTestCase(FINGERPRINT_STATUS_UI_BASE_URL, new String[] { INVALID_FINGERPRINT }, null)
            .setResponseContentType(ContentType.HTML)
            .setStatusCode(404).build();

    @Test
    void testGetValidationStatus_success_validFormatAndSchema() {
        EndpointTestBuilder.from(GET_STATUS_SUCCESS_CASE).doGet().andCheckSchema().andCheckFormat().run();
    }

    @Test
    void testGetValidationStatus_success_invalidFingerprint() {
        // invalid/not found fingerprint returns 200 with an empty list
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FINGERPRINT_STATUS_BASE_URL, new String[] { INVALID_FINGERPRINT }, null)
                        .setBodyValidationParams(Map.of("", Collections.emptyList())).build())
                .andCheckBodyParams()
                .doGet()
                .run();
    }

    @Test
    void testGetValidationStatusUi_success() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FINGERPRINT_STATUS_UI_BASE_URL, new String[] { VALID_FINGERPRINT }, null)
                        .setResponseContentType(ContentType.HTML).build())
                .doGet().run();
    }

    @Test
    void testGetValidationStatusUi_failure_notFound() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FINGERPRINT_STATUS_UI_BASE_URL, new String[] { INVALID_FINGERPRINT }, null)
                        .setResponseContentType(ContentType.HTML).setStatusCode(404).build())
                .doGet().run();
    }
}
