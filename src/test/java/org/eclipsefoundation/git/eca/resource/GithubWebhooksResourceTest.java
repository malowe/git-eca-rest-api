/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.resource;

import java.util.Map;
import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipsefoundation.core.exception.ApplicationException;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.Installation;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.PullRequest;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.PullRequestHead;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.Repository;
import org.eclipsefoundation.git.eca.namespace.WebhookHeaders;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class GithubWebhooksResourceTest {
    private static final String GH_WEBHOOK_BASE_URL = "/webhooks/github";

    @Inject
    ObjectMapper om;

    @Test
    void testInvalidEventType() {
        EndpointTestBuilder.from(TestCaseHelper.prepareTestCase(GH_WEBHOOK_BASE_URL, new String[] {}, null)
                .setHeaderParams(Optional.of(Map.of(WebhookHeaders.GITHUB_EVENT, "nope"))).build()).doPost()
                .run();
    }

    @Test
    void testGHWebhook_success() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(GH_WEBHOOK_BASE_URL, new String[] {}, null)
                        .setHeaderParams(Optional.of(Map.of(WebhookHeaders.GITHUB_DELIVERY, "id-1", WebhookHeaders.GITHUB_EVENT, "pull_request")))
                        .build())
                .doPost(createGHWebhook())
                .run();
    }

    private String createGHWebhook() {
        try {
            return om.writeValueAsString(GithubWebhookRequest.builder()
                    .setInstallation(Installation.builder().setId("install-id").build())
                    .setPullRequest(PullRequest
                            .builder()
                            .setNumber(42)
                            .setHead(PullRequestHead.builder().setSha("sha-1234").build())
                            .setState("open")
                            .build())
                    .setRepository(Repository
                            .builder()
                            .setFullName("eclipsefdn/sample")
                            .setHtmlUrl("http://www.github.com/eclipsefdn/sample")
                            .build())
                    .build());
        } catch (Exception e) {
            throw new ApplicationException("Error converting Hook to JSON");
        }
    }
}
