/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.api.ProjectsAPI;
import org.eclipsefoundation.efservices.api.models.GitlabProject;
import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.eclipsefoundation.efservices.api.models.InterestGroup.Descriptor;
import org.eclipsefoundation.efservices.api.models.InterestGroup.InterestGroupParticipant;
import org.eclipsefoundation.efservices.api.models.InterestGroup.Organization;
import org.eclipsefoundation.efservices.api.models.InterestGroup.Resource;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.efservices.api.models.Project.GithubProject;
import org.eclipsefoundation.efservices.api.models.Project.ProjectParticipant;
import org.eclipsefoundation.efservices.api.models.Project.Repo;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.Response;

@Mock
@RestClient
@ApplicationScoped
public class MockProjectsAPI implements ProjectsAPI {

    private List<Project> projects;

    public MockProjectsAPI() {
        this.projects = new ArrayList<>();

        // sample repos
        Repo r1 = Repo.builder().setUrl("http://www.github.com/eclipsefdn/sample").build();
        Repo r2 = Repo.builder().setUrl("http://www.github.com/eclipsefdn/test").build();
        Repo r3 = Repo.builder().setUrl("http://www.github.com/eclipsefdn/prototype.git").build();
        Repo r4 = Repo.builder().setUrl("http://www.github.com/eclipsefdn/tck-proto").build();
        Repo r5 = Repo.builder().setUrl("/gitroot/sample/gerrit.project.git").build();
        Repo r6 = Repo.builder().setUrl("/gitroot/sample/gerrit.other-project").build();

        // sample users
        ProjectParticipant u1 = ProjectParticipant.builder().setUrl("").setUsername("da_wizz").setFullName("da_wizz")
                .build();
        ProjectParticipant u2 = ProjectParticipant.builder().setUrl("").setUsername("grunter").setFullName("grunter")
                .build();

        this.projects.add(Project.builder()
                .setName("Sample project")
                .setProjectId("sample.proj")
                .setSpecProjectWorkingGroup(Collections.emptyMap())
                .setGithubRepos(Arrays.asList(r1, r2))
                .setGerritRepos(Arrays.asList(r5))
                .setCommitters(Arrays.asList(u1, u2))
                .setProjectLeads(Collections.emptyList())
                .setGitlab(GitlabProject.builder().setIgnoredSubGroups(Collections.emptyList()).setProjectGroup("")
                        .build())
                .setShortProjectId("sample.proj")
                .setSummary("summary")
                .setUrl("project.url.com")
                .setWebsiteUrl("someproject.com")
                .setWebsiteRepo(Collections.emptyList())
                .setLogo("logoUrl.com")
                .setTags(Collections.emptyList())
                .setGithub(GithubProject.builder()
                        .setOrg("")
                        .setIgnoredRepos(Collections.emptyList()).build())
                .setContributors(Collections.emptyList())
                .setWorkingGroups(Collections.emptyList())
                .setIndustryCollaborations(Collections.emptyList())
                .setReleases(Collections.emptyList())
                .setTopLevelProject("eclipse")
                .build());

        this.projects.add(Project.builder()
                .setName("Prototype thing")
                .setProjectId("sample.proto")
                .setSpecProjectWorkingGroup(Collections.emptyMap())
                .setGithubRepos(Arrays.asList(r3))
                .setGerritRepos(Arrays.asList(r6))
                .setCommitters(Arrays.asList(u2))
                .setProjectLeads(Collections.emptyList())
                .setGitlab(
                        GitlabProject.builder().setIgnoredSubGroups(Collections.emptyList())
                                .setProjectGroup("eclipse/dash-second").build())
                .setShortProjectId("sample.proto")
                .setWebsiteUrl("someproject.com")
                .setSummary("summary")
                .setUrl("project.url.com")
                .setWebsiteRepo(Collections.emptyList())
                .setLogo("logoUrl.com")
                .setTags(Collections.emptyList())
                .setGithub(GithubProject.builder()
                        .setOrg("")
                        .setIgnoredRepos(Collections.emptyList()).build())
                .setContributors(Collections.emptyList())
                .setWorkingGroups(Collections.emptyList())
                .setIndustryCollaborations(Collections.emptyList())
                .setReleases(Collections.emptyList())
                .setTopLevelProject("eclipse")
                .build());

        this.projects.add(Project.builder()
                .setName("Spec project")
                .setProjectId("spec.proj")
                .setSpecProjectWorkingGroup(Map.of("id", "proj1", "name", "proj1"))
                .setGithubRepos(Arrays.asList(r4))
                .setGerritRepos(Collections.emptyList())
                .setGitlab(GitlabProject
                        .builder()
                        .setIgnoredSubGroups(Arrays.asList("eclipse/dash/mirror"))
                        .setProjectGroup("eclipse/dash")
                        .build())
                .setCommitters(Arrays.asList(u1, u2))
                .setProjectLeads(Collections.emptyList())
                .setShortProjectId("spec.proj")
                .setSummary("summary")
                .setUrl("project.url.com")
                .setWebsiteUrl("someproject.com")
                .setWebsiteRepo(Collections.emptyList())
                .setLogo("logoUrl.com")
                .setTags(Collections.emptyList())
                .setGithub(GithubProject.builder()
                        .setOrg("eclipsefdn-tck")
                        .setIgnoredRepos(Arrays.asList("eclipsefdn-tck/tck-ignored")).build())
                .setContributors(Collections.emptyList())
                .setWorkingGroups(Collections.emptyList())
                .setIndustryCollaborations(Collections.emptyList())
                .setReleases(Collections.emptyList())
                .setTopLevelProject("eclipse")
                .build());
    }

    @Override
    public Response getProjects(BaseAPIParameters params, int isSpecProject) {

        if (isSpecProject == 1) {
            return Response
                    .ok(projects.stream().filter(p -> p.getSpecWorkingGroup().isPresent()).toList())
                    .build();
        }

        return Response.ok(projects).build();
    }

    @Override
    public Response getInterestGroups(BaseAPIParameters params) {
        return Response.ok(Arrays.asList(InterestGroup
                .builder()
                .setProjectId("foundation-internal.ig.mittens")
                .setId("1")
                .setLogo("")
                .setState("active")
                .setTitle("Magical IG Tributed To Eclipse News Sources")
                .setDescription(Descriptor.builder().setFull("Sample").setSummary("Sample").build())
                .setScope(Descriptor.builder().setFull("Sample").setSummary("Sample").build())
                .setGitlab(GitlabProject.builder()
                        .setIgnoredSubGroups(Collections.emptyList())
                        .setProjectGroup("eclipse-ig/mittens")
                        .build())
                .setLeads(Arrays.asList(InterestGroupParticipant
                        .builder()
                        .setUrl("https://api.eclipse.org/account/profile/zacharysabourin")
                        .setUsername("zacharysabourin")
                        .setFullName("zachary sabourin")
                        .setOrganization(Organization.builder()
                                .setDocuments(Collections.emptyMap())
                                .setId("id")
                                .setName("org").build())
                        .build()))
                .setParticipants(Arrays.asList(InterestGroupParticipant
                        .builder()
                        .setUrl("https://api.eclipse.org/account/profile/skilpatrick")
                        .setUsername("skilpatrick")
                        .setFullName("Skil Patrick")
                        .setOrganization(Organization.builder()
                                .setDocuments(Collections.emptyMap())
                                .setId("id")
                                .setName("org").build())
                        .build()))
                .setShortProjectId("mittens")
                .setResources(Resource.builder().setMembers("members").setWebsite("google.com").build())
                .setMailingList("mailinglist.com")
                .build())).build();
    }
}
