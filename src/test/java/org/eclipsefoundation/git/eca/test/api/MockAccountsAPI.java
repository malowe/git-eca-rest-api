/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.test.api;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.git.eca.api.AccountsAPI;
import org.eclipsefoundation.git.eca.api.models.EclipseUser;
import org.eclipsefoundation.git.eca.api.models.EclipseUser.ECA;

import io.quarkus.test.Mock;

/**
 * Simple stub for accounts API. Allows for easy testing of users that don't really exist upstream, and so that we don't need a real auth
 * token for data.
 * 
 * @author Martin Lowe
 *
 */
@Mock
@RestClient
@ApplicationScoped
public class MockAccountsAPI implements AccountsAPI {

    private Map<String, EclipseUser> users;

    public MockAccountsAPI() {
        int id = 0;
        this.users = new HashMap<>();
        users
                .put("newbie@important.co",
                        EclipseUser
                                .builder()
                                .setIsCommitter(false)
                                .setUid(id++)
                                .setMail("newbie@important.co")
                                .setName("newbieAnon")
                                .setECA(ECA.builder().build())
                                .build());
        users
                .put("slom@eclipse-foundation.org",
                        EclipseUser
                                .builder()
                                .setIsCommitter(false)
                                .setUid(id++)
                                .setMail("slom@eclipse-foundation.org")
                                .setName("barshall_blathers")
                                .setECA(ECA.builder().setCanContributeSpecProject(true).setSigned(true).build())
                                .build());
        users
                .put("tester@eclipse-foundation.org",
                        EclipseUser
                                .builder()
                                .setIsCommitter(false)
                                .setUid(id++)
                                .setMail("tester@eclipse-foundation.org")
                                .setName("mctesterson")
                                .setECA(ECA.builder().setCanContributeSpecProject(false).setSigned(true).build())
                                .build());
        users
                .put("code.wiz@important.co",
                        EclipseUser
                                .builder()
                                .setIsCommitter(true)
                                .setUid(id++)
                                .setMail("code.wiz@important.co")
                                .setName("da_wizz")
                                .setECA(ECA.builder().setCanContributeSpecProject(true).setSigned(true).build())
                                .setGithubHandle("wiz_in_da_hub")
                                .build());
        users
                .put("grunt@important.co",
                        EclipseUser
                                .builder()
                                .setIsCommitter(true)
                                .setUid(id++)
                                .setMail("grunt@important.co")
                                .setName("grunter")
                                .setECA(ECA.builder().setCanContributeSpecProject(false).setSigned(true).build())
                                .setGithubHandle("grunter2")
                                .build());
        users
                .put("paper.pusher@important.co",
                        EclipseUser
                                .builder()
                                .setIsCommitter(false)
                                .setUid(id++)
                                .setMail("paper.pusher@important.co")
                                .setName("sumAnalyst")
                                .setECA(ECA.builder().setCanContributeSpecProject(false).setSigned(true).build())
                                .build());
        users
                .put("opearson@important.co",
                        EclipseUser
                                .builder()
                                .setIsCommitter(true)
                                .setUid(id++)
                                .setMail("opearson@important.co")
                                .setName("opearson")
                                .setECA(ECA.builder().setCanContributeSpecProject(false).setSigned(true).build())
                                .setGithubHandle("opearson")
                                .setIsCommitter(true)
                                .build());
    }

    @Override
    public List<EclipseUser> getUsers(String token, String mail) {
        return Arrays.asList(users.get(mail));
    }

    @Override
    public EclipseUser getUserByGithubUname(String token, String uname) {
        // assumes github id is same as uname for purposes of lookup (simplifies fetch logic)
        return users
                .values()
                .stream()
                .filter(u -> u.getGithubHandle() != null && u.getGithubHandle().equals(uname))
                .findFirst()
                .orElseGet(() -> null);
    }

}
