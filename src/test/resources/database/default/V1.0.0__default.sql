CREATE TABLE CommitValidationStatus (
  commitHash varchar(100) NOT NULL,
  project varchar(100) DEFAULT NULL,
  lastModified datetime DEFAULT NULL,
  creationDate datetime DEFAULT NULL,
  id bigint NOT NULL AUTO_INCREMENT,
  provider varchar(100) NOT NULL,
  estimatedLoc int DEFAULT 0,
  repoUrl varchar(255) NOT NULL,
  userMail varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);
INSERT INTO CommitValidationStatus(commitHash,project,lastModified,creationDate,provider, repoUrl) VALUES('123456789', 'sample.proj', NOW(), NOW(),'GITHUB','http://www.github.com/eclipsefdn/sample');
INSERT INTO CommitValidationStatus(commitHash,project,lastModified,creationDate,provider, repoUrl) VALUES('123456789', 'sample.proto', NOW(), NOW(),'GITLAB','');
INSERT INTO CommitValidationStatus(commitHash,project,lastModified,creationDate,provider, repoUrl) VALUES('987654321', 'sample.proto', NOW(), NOW(),'GITLAB','');
INSERT INTO CommitValidationStatus(commitHash,lastModified,creationDate,provider, repoUrl) VALUES('abc123def456', NOW(), NOW(),'GITHUB','http://www.github.com/eclipsefdn/sample');
INSERT INTO CommitValidationStatus(id,commitHash,lastModified,creationDate,provider, repoUrl) VALUES(999,'duplicate-message-err-02', NOW(), NOW(),'GITHUB','http://www.github.com/eclipsefdn/test');
INSERT INTO CommitValidationStatus(id,commitHash,lastModified,creationDate,provider, repoUrl) VALUES(998,'deduplicate-message-test-01', NOW(), NOW(),'GITHUB','http://www.github.com/eclipsefdn/test');

CREATE TABLE CommitValidationMessage (
  providerId varchar(100) DEFAULT NULL,
  authorEmail varchar(100) DEFAULT NULL,
  committerEmail varchar(100) DEFAULT NULL,
  eclipseId varchar(255) DEFAULT NULL,
  id bigint NOT NULL AUTO_INCREMENT,
  commit_id int NOT NULL,
  statusCode int NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO CommitValidationMessage(commit_id,providerId,authorEmail,committerEmail,eclipseId,statusCode) VALUES(1,'','','','',-405);
INSERT INTO CommitValidationMessage(commit_id,providerId,authorEmail,committerEmail,eclipseId,statusCode) VALUES(999,'','newbie@important.co','newbie@important.co','',-404);
INSERT INTO CommitValidationMessage(commit_id,providerId,authorEmail,committerEmail,eclipseId,statusCode) VALUES(999,'','newbie@important.co','newbie@important.co','',-404);
INSERT INTO CommitValidationMessage(commit_id,providerId,authorEmail,committerEmail,eclipseId,statusCode) VALUES(998,'','newbie@important.co','newbie@important.co','',-404);
INSERT INTO CommitValidationMessage(commit_id,providerId,authorEmail,committerEmail,eclipseId,statusCode) VALUES(998,'','newbie@important.co','newbie@important.co','',-404);

CREATE TABLE `CommitValidationStatusGrouping` (
  `fingerprint` varchar(255) NOT NULL,
  `commit_id` bigint NOT NULL,
  PRIMARY KEY (`commit_id`,`fingerprint`)
);
INSERT INTO CommitValidationStatusGrouping(fingerprint, commit_id) VALUES('957706b0f31e0ccfc5287c0ebc62dc79', 1);
INSERT INTO CommitValidationStatusGrouping(fingerprint, commit_id) VALUES('deduplicate-message-test-fingerprint', 998);

CREATE TABLE PrivateProjectEvent (
  userId int NOT NULL,
  projectId int NOT NULL,
  projectPath varchar(255) NOT NULL,
  ef_username varchar(100) DEFAULT NULL,
  parentProject int DEFAULT NULL,
  creationDate datetime NOT NULL,
  deletionDate datetime DEFAULT NULL,
  PRIMARY KEY (userId, projectId, projectPath)
);
INSERT INTO PrivateProjectEvent(userId, projectId, projectPath, ef_username, creationDate) VALUES(1, 133, 'eclipse/test/project', 'testtesterson', '2022-11-11T12:00:00.000');
INSERT INTO PrivateProjectEvent(userId, projectId, projectPath, ef_username, parentProject, creationDate, deletionDate) VALUES(1, 150, 'eclipse/test/project/fork', 'tyronetesty', 133, '2022-11-15T12:00:00.000', '2022-11-20T12:00:00.000');

CREATE TABLE GithubWebhookTracking (
  id SERIAL NOT NULL,
  lastKnownState varchar(63) NOT NULL,
  installationId varchar(63) NOT NULL,
  headSha varchar(63) NOT NULL,
  repositoryFullName varchar(127) NOT NULL,
  pullRequestNumber int NOT NULL,
  lastUpdated datetime DEFAULT NULL,
  needsRevalidation BOOLEAN DEFAULT FALSE,
  manualRevalidationCount int DEFAULT 0,
  PRIMARY KEY (id)
);