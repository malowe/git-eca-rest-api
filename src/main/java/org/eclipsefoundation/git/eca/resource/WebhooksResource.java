/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.resource;

import jakarta.inject.Inject;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.git.eca.api.models.SystemHook;
import org.eclipsefoundation.git.eca.namespace.EventType;
import org.eclipsefoundation.git.eca.namespace.WebhookHeaders;
import org.eclipsefoundation.git.eca.service.SystemHookService;
import org.jboss.resteasy.annotations.jaxrs.HeaderParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("webhooks/gitlab")
public class WebhooksResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebhooksResource.class);

    @Inject
    RequestWrapper wrapper;
    @Inject
    SystemHookService hookService;

    @Inject
    ObjectMapper om;

    @POST
    @Path("system")
    public Response processGitlabHook(@HeaderParam(WebhookHeaders.GITLAB_EVENT) String eventHeader, String jsonBody) {
        // Do not process if header is incorrect
        if (!"system hook".equalsIgnoreCase(eventHeader)) {
            return Response.ok().build();
        }

        // based on event name in body, process the event hook
        String eventName = readEventName(jsonBody);
        EventType type = EventType.getType(eventName);
        switch (type) {
            case PROJECT_CREATE:
                hookService.processProjectCreateHook(wrapper, convertRequestToHook(jsonBody));
                break;
            case PROJECT_DESTROY:
                hookService.processProjectDeleteHook(wrapper, convertRequestToHook(jsonBody));
                break;
            case PROJECT_RENAME:
                hookService.processProjectRenameHook(wrapper, convertRequestToHook(jsonBody));
                break;
            case UNSUPPORTED:
            default:
                LOGGER.trace("Dropped event: {}", eventName);
                break;
        }

        return Response.ok().build();
    }

    /**
     * Processes the json body contents and converts them to a SystemHook object. Returns null if the json body couldn't be
     * processed.
     * 
     * @param jsonBody the json body as a string
     * @return A SystemHook object converted from a json string
     */
    private SystemHook convertRequestToHook(String jsonBody) {
        try {
            return om.readerFor(SystemHook.class).readValue(jsonBody);
        } catch (JsonProcessingException e) {
            LOGGER.error("Error converting JSON body into system hook", e);
            return null;
        }
    }

    /**
     * Processes the json body contents and returns the event_name field. Returns null if the was a processing error or if
     * the event_name field is null/missing.
     * 
     * @param jsonBody the json body as a String
     * @return the event_name field
     */
    private String readEventName(String jsonBody) {
        try {
            return om.readTree(jsonBody).path("event_name").asText();
        } catch (JsonProcessingException e) {
            LOGGER.error("Error reading event_name from JSON body", e);
            return null;
        }
    }
}
