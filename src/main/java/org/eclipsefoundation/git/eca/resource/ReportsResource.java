/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.resource;

import java.time.LocalDate;

import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.exception.UnauthorizedException;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.git.eca.config.EcaReportsConfig;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.service.ReportsService;

@Path("/reports")
public class ReportsResource {
    @Inject
    EcaReportsConfig config;

    @Inject
    RequestWrapper wrap;
    @Inject
    ReportsService reportsService;

    @GET
    @Path("/gitlab/private-projects")
    public Response getPrivateProjectEvents(@QueryParam("key") String passedKey, @QueryParam("status") String status,
            @QueryParam("since") LocalDate since, @QueryParam("until") LocalDate until) {
        if (!config.accessKey().equals(passedKey)) {
            throw new UnauthorizedException("Bad key passed for report access, access blocked");
        }
        if (StringUtils.isNotBlank(status) && !isValidStatus(status)) {
            throw new BadRequestException(String.format("Invalid 'status' parameter: %s", status));
        }

        return Response.ok(reportsService.getPrivateProjectEvents(wrap, status, since, until)).build();
    }

    /**
     * Validates the status as one of "active" or "deleted"
     * 
     * @param status the desired status
     * @return true if valid, false if not
     */
    private boolean isValidStatus(String status) {
        return status.equalsIgnoreCase(GitEcaParameterNames.STATUS_ACTIVE.getName())
                || status.equalsIgnoreCase(GitEcaParameterNames.STATUS_DELETED.getName());
    }
}
