/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.resource;

import java.util.Arrays;

import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.git.eca.api.models.EclipseUser;
import org.eclipsefoundation.git.eca.dto.GithubWebhookTracking;
import org.eclipsefoundation.git.eca.service.UserService;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;

import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;
import jakarta.inject.Inject;

/**
 * Contains operations and properties that are common to resources that interact with Github validation and user accounts.
 * 
 * @author Martin Lowe
 *
 */
public abstract class CommonResource {

    @Inject
    UserService users;
    @Inject
    PersistenceDao dao;
    @Inject
    FilterService filters;

    @Inject
    RequestWrapper wrapper;
    @Inject
    SecurityIdentity ident;

    void setRevalidationFlagForTracking(GithubWebhookTracking tracking) {
        tracking.setNeedsRevalidation(true);
        tracking.setLastUpdated(DateTimeHelper.now());
        dao.add(new RDBMSQuery<>(wrapper, filters.get(GithubWebhookTracking.class)), Arrays.asList(tracking));
    }

    EclipseUser getUserForLoggedInAccount() {
        if (ident.isAnonymous()) {
            return null;
        }
        // cast to a principal w/ access to claims
        DefaultJWTCallerPrincipal defaultPrin = (DefaultJWTCallerPrincipal) ident.getPrincipal();
        // get the user account linked to the current claim
        return users.getUser(defaultPrin.getClaim("email"));
    }
}
