/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*	      Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.exception.FinalForbiddenException;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.git.eca.api.models.EclipseUser;
import org.eclipsefoundation.git.eca.helper.ProjectHelper;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.model.ValidationResponse;
import org.eclipsefoundation.git.eca.namespace.APIStatusCode;
import org.eclipsefoundation.git.eca.service.ValidationService;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.Authenticated;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * ECA validation endpoint for Git commits. Will use information from the bots, projects, and accounts API to validate commits passed to
 * this endpoint. Should be as system agnostic as possible to allow for any service to request validation with less reliance on services
 * external to the Eclipse foundation.
 *
 * @author Martin Lowe, Zachary Sabourin
 */
@Path("/eca")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class ValidationResource extends CommonResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationResource.class);

    // external API/service harnesses
    @Inject
    CachingService cache;
    @Inject
    ProjectHelper projects;
    @Inject
    ValidationService validation;

    /**
     * Consuming a JSON request, this method will validate all passed commits, using the repo URL and the repository provider. These commits
     * will be validated to ensure that all users are covered either by an ECA, or are committers on the project. In the case of ECA-only
     * contributors, an additional sign off footer is required in the body of the commit.
     *
     * @param req the request containing basic data plus the commits to be validated
     * @return a web response indicating success or failure for each commit, along with standard messages that may be used to give users
     * context on failure.
     */
    @POST
    public Response validate(ValidationRequest req) {
        List<String> messages = checkRequest(req);
        // only process if we have no errors
        if (messages.isEmpty()) {
            LOGGER.debug("Processing: {}", req);
            return validation.validateIncomingRequest(req, wrapper).toResponse();
        } else {
            // create a stubbed response with the errors
            ValidationResponse out = ValidationResponse.builder().build();
            messages.forEach(m -> out.addError(m, null, APIStatusCode.ERROR_DEFAULT));
            return out.toResponse();
        }
    }

    @GET
    @Path("/lookup")
    @Authenticated
    public Response getUserStatus(@QueryParam("email") String email) {
        // check that the user has committer level access
        EclipseUser loggedInUser = getUserForLoggedInAccount();
        if (loggedInUser == null || !loggedInUser.getIsCommitter()) {
            throw new FinalForbiddenException("User must be logged in and have committer level access to use this endpoint.");
        }
        // do the lookup of the passed email
        EclipseUser user = users.getUser(email);
        if (Objects.isNull(user)) {
            throw new NotFoundException(String.format("No user found with mail '%s'", TransformationHelper.formatLog(email)));
        }

        if (!user.getECA().getSigned()) {
            throw new FinalForbiddenException("");
        }
        return Response.ok().build();
    }

    /**
     * Check if there are any issues with the validation request, returning error messages if there are issues with the request.
     * 
     * @param req the current validation request
     * @return a list of error messages to report, or an empty list if there are no errors with the request.
     */
    private List<String> checkRequest(ValidationRequest req) {
        // check that we have commits to validate
        List<String> messages = new ArrayList<>();
        if (req.getCommits() == null || req.getCommits().isEmpty()) {
            messages.add("A commit is required to validate");
        }
        // check that we have a repo set
        if (req.getRepoUrl() == null || StringUtils.isBlank(req.getRepoUrl().getPath())) {
            messages.add("A base repo URL needs to be set in order to validate");
        }
        // check that we have a type set
        if (req.getProvider() == null) {
            messages.add("A provider needs to be set to validate a request");
        }
        return messages;
    }

}
