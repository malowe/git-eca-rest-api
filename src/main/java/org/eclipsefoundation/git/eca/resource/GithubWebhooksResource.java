/**
 * Copyright (c) 2022, 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.apache.http.HttpStatus;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.PullRequest;
import org.eclipsefoundation.git.eca.dto.GithubWebhookTracking;
import org.eclipsefoundation.git.eca.helper.CaptchaHelper;
import org.eclipsefoundation.git.eca.helper.GithubValidationHelper;
import org.eclipsefoundation.git.eca.model.RevalidationResponse;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.namespace.HCaptchaErrorCodes;
import org.eclipsefoundation.git.eca.namespace.WebhookHeaders;
import org.eclipsefoundation.git.eca.service.GithubApplicationService;
import org.jboss.resteasy.annotations.jaxrs.HeaderParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.ServerErrorException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * Resource for processing Github pull request events, used to update commit status entries for the Git ECA application.
 * 
 * @author Martin Lowe
 *
 */
@Path("webhooks/github")
public class GithubWebhooksResource extends CommonResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(GithubWebhooksResource.class);

    @Inject
    GithubApplicationService ghAppService;

    @Inject
    GithubValidationHelper validationHelper;
    @Inject
    CaptchaHelper captchaHelper;

    /**
     * Entry point for processing Github webhook requests. Accepts standard fields as described in the <a href=
     * "https://docs.github.com/developers/webhooks-and-events/webhooks/webhook-events-and-payloads#webhook-payload-object-common-properties">Webhook
     * properties documentation</a>, as well as the <a href=
     * "https://docs.github.com/developers/webhooks-and-events/webhooks/webhook-events-and-payloads#pull_request">Pull
     * Request event type documentation.</a>
     * 
     * @param deliveryId Github provided unique delivery ID
     * @param eventType the type of webhook event that was fired
     * @param request the webhook request body.
     * @return an OK status when done processing.
     */
    @POST
    public Response processGithubWebhook(@HeaderParam(WebhookHeaders.GITHUB_DELIVERY) String deliveryId,
            @HeaderParam(WebhookHeaders.GITHUB_EVENT) String eventType, GithubWebhookRequest request) {
        // If event isn't a pr event, drop as we don't process them
        if (!"pull_request".equalsIgnoreCase(eventType)) {
            return Response.ok().build();
        }

        // check that the pull request isn't somehow missing
        if (request.getPullRequest() == null) {
            throw new BadRequestException("Pull request event submitted, but pull request information was missing");
        }
        PullRequest pr = request.getPullRequest();

        // track the request before we start processing
        GithubWebhookTracking tracking = validationHelper
                .retrieveAndUpdateTrackingInformation(wrapper, request.getInstallation().getId(), request.getRepository().getFullName(),
                        pr);

        // start processing the request
        LOGGER.trace("Processing PR event for install {} with delivery ID of {}", request.getInstallation().getId(), deliveryId);
        try {
            // prepare for validation process by pre-processing into standard format
            ValidationRequest vr = generateRequest(request, pr);
            // process the request
            validationHelper.handleGithubWebhookValidation(request, vr, wrapper);
        } catch (Exception e) {
            // set the revalidation flag to reprocess if the initial attempt fails before exiting
            setRevalidationFlagForTracking(tracking);
        }

        return Response.ok().build();
    }

    @GET
    @Path("installations")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getManagedInstallations() {
        return Response.ok(ghAppService.getManagedInstallations()).build();
    }

    /**
     * Endpoint for triggering revalidation of a past request. Uses the fingerprint hash to lookup the unique request and to
     * rebuild the request and revalidate if it exists.
     * 
     * @param fullRepoName the full repository name for the target repo, e.g. eclipse/jetty
     * @param installationId the installation ID for the ECA app in the given repository
     * @param prNo the pull request number to be revalidated
     * @param captchaResponse the passed captcha challenge response
     * @return redirect to the pull request once done processing
     */
    @POST
    @Path("revalidate")
    public Response revalidateWebhookRequest(@QueryParam(GitEcaParameterNames.REPOSITORY_FULL_NAME_RAW) String fullRepoName,
            @QueryParam(GitEcaParameterNames.INSTALLATION_ID_RAW) String installationId,
            @QueryParam(GitEcaParameterNames.PULL_REQUEST_NUMBER_RAW) Integer prNo,
            @FormParam("h-captcha-response") String captchaResponse) {
        String sanitizedRepoName = TransformationHelper.formatLog(fullRepoName);
        // retrieve and check that the PR exists
        Optional<PullRequest> prResponse = ghAppService.getPullRequest(installationId, fullRepoName, prNo);
        if (prResponse.isEmpty()) {
            throw new NotFoundException(
                    String.format("Cannot find Github PR for repo '%s', pull request number '%d'", sanitizedRepoName, prNo));
        }

        // get the tracking if it exists, create it if it doesn't, and fail out if there is an issue
        GithubWebhookTracking tracking = validationHelper
                .retrieveAndUpdateTrackingInformation(wrapper, installationId, fullRepoName, prResponse.get());
        if (tracking == null) {
            throw new ServerErrorException(String
                    .format("Cannot find a tracked pull request with for repo '%s', pull request number '%d'", sanitizedRepoName, prNo),
                    HttpStatus.SC_INTERNAL_SERVER_ERROR);
        } else if (!"open".equalsIgnoreCase(tracking.getLastKnownState())) {
            // we do not want to reprocess non-open pull requests
            throw new BadRequestException("Cannot revalidate a non-open pull request");
        }

        // check the captcha challenge response
        List<HCaptchaErrorCodes> errors = captchaHelper.validateCaptchaResponse(captchaResponse);
        if (!errors.isEmpty()) {
            // use debug logging as this could be incredibly noisy
            LOGGER
                    .debug("Captcha challenge failed with the following errors for revalidation request for '{}#{}': {}", sanitizedRepoName,
                            prNo, errors.stream().map(HCaptchaErrorCodes::getMessage));
            throw new BadRequestException("hCaptcha challenge response failed for this request");
        }

        // wrap the processing in a try-catch to catch upstream errors and recast them as server errors
        try {
            // get the tracking class, convert back to a GH webhook request, and validate the request
            GithubWebhookRequest request = GithubWebhookRequest.buildFromTracking(tracking);
            if (request.getPullRequest() == null) {
                throw new ServerErrorException("Current validation is in a bad state, pull request information missing",
                        Status.INTERNAL_SERVER_ERROR);
            }
            boolean isSuccessful = validationHelper
                    .handleGithubWebhookValidation(request, generateRequest(request, request.getPullRequest()), wrapper)
                    .getPassed();
            LOGGER.debug("Revalidation for request for '{}#{}' was {}successful.", sanitizedRepoName, prNo, isSuccessful ? "" : " not");

            // build the url for pull request page
            StringBuilder sb = new StringBuilder();
            sb.append("https://github.com/");
            sb.append(fullRepoName);
            sb.append("/pull/");
            sb.append(prNo);
            // respond with a URL to the new location in a standard request
            return Response.ok(RevalidationResponse.builder().setLocation(URI.create(sb.toString())).build()).build();
        } catch (Exception e) {
            // rewrap exception, as some of the Github stuff can fail w/o explanation
            throw new ServerErrorException(HttpStatus.SC_INTERNAL_SERVER_ERROR, e);
        }
    }

    /**
     * Generate the validation request for the current GH Webhook request.
     * 
     * @param request the current webhook request for validation
     * @param pr null checked pull request from Github
     * @return the Validation Request body to be used in validating data
     */
    private ValidationRequest generateRequest(GithubWebhookRequest request, PullRequest pr) {
        return validationHelper
                .generateRequest(request.getInstallation().getId(), request.getRepository().getFullName(), pr.getNumber(),
                        request.getRepository().getHtmlUrl());
    }

}
