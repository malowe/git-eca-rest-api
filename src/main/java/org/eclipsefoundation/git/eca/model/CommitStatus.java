/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.annotation.Nullable;

import org.eclipsefoundation.git.eca.namespace.APIStatusCode;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Contains information generated about a commit that was submitted for
 * validation to the API.
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_CommitStatus.Builder.class)
public abstract class CommitStatus {
    public abstract List<CommitStatusMessage> getMessages();

    public abstract List<CommitStatusMessage> getWarnings();

    public abstract List<CommitStatusMessage> getErrors();

    public void addMessage(String message, APIStatusCode code) {
        this.getMessages().add(CommitStatusMessage.builder().setCode(code).setMessage(message).build());
    }

    public void addWarning(String message, APIStatusCode code) {
        this.getWarnings().add(CommitStatusMessage.builder().setCode(code).setMessage(message).build());
    }

    public void addError(String message, APIStatusCode code) {
        this.getErrors().add(CommitStatusMessage.builder().setCode(code).setMessage(message).build());
    }

    public static Builder builder() {
        return new AutoValue_CommitStatus.Builder().setErrors(new ArrayList<>()).setWarnings(new ArrayList<>())
                .setMessages(new ArrayList<>());
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setMessages(List<CommitStatusMessage> messages);

        public abstract Builder setWarnings(List<CommitStatusMessage> warnings);

        public abstract Builder setErrors(List<CommitStatusMessage> errors);

        public abstract CommitStatus build();
    }

    /**
     * Represents a message with an associated error or success status code.
     * 
     * @author Martin Lowe
     *
     */
    @AutoValue
    @JsonDeserialize(builder = AutoValue_CommitStatus_CommitStatusMessage.Builder.class)
    public abstract static class CommitStatusMessage {
        public abstract APIStatusCode getCode();

        @Nullable
        public abstract String getMessage();

        public static Builder builder() {
            return new AutoValue_CommitStatus_CommitStatusMessage.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setCode(APIStatusCode code);

            public abstract Builder setMessage(@Nullable String message);

            public abstract CommitStatusMessage build();
        }
    }
}
