/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.model.mappers;

import org.eclipsefoundation.git.eca.dto.PrivateProjectEvent;
import org.eclipsefoundation.git.eca.model.PrivateProjectData;
import org.eclipsefoundation.persistence.config.QuarkusMappingConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = QuarkusMappingConfig.class)
public interface PrivateProjectEventMapper extends BaseEntityMapper<PrivateProjectEvent, PrivateProjectData> {

    @Mapping(source = "compositeId.userId", target = "userId")
    @Mapping(source = "compositeId.projectId", target = "projectId")
    @Mapping(source = "compositeId.projectPath", target = "projectPath")
    PrivateProjectData toModel(PrivateProjectEvent dto);
}
