/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.tasks;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.control.ActivateRequestContext;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.git.eca.dto.GithubWebhookTracking;
import org.eclipsefoundation.git.eca.helper.GithubValidationHelper;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.namespace.PersistenceUrlParameterNames;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.scheduler.Scheduled;

/**
 * Scheduled regular task that will interact with the backend persistence to look for requests that are in a
 * failed/unvalidated state after an error while processing that could not be recovered. These requests will be
 * reprocessed using the same logic as the standard validation, updating the timestamp on completion and either setting
 * the revalidation flag to false or incrementing the number of repeated revalidations needed for the request for
 * tracking, depending on the succcess of the revalidation.
 */
@ApplicationScoped
public class GithubRevalidationQueue {
    private static final Logger LOGGER = LoggerFactory.getLogger(GithubRevalidationQueue.class);

    // full repo name should be 2 parts, org and actual repo name
    private static final int FULL_REPO_NAME_PARTS = 2;

    @ConfigProperty(name = "eclipse.git-eca.tasks.gh-revalidation.enabled", defaultValue = "true")
    Instance<Boolean> isEnabled;

    @Inject
    PersistenceDao dao;
    @Inject
    FilterService filters;

    @Inject
    GithubValidationHelper validationHelper;

    @PostConstruct
    void init() {
        // indicate to log whether enabled to reduce log spam
        LOGGER.info("Github revalidation queue task is{} enabled.", Boolean.TRUE.equals(isEnabled.get()) ? "" : " not");
    }

    /**
     * Every 5s, this method will attempt to load a Github webhook validation request that has the needs revalidation flag
     * set to true. This will retrieve the oldest request in queue and will attempt to revalidate it.
     */
    @Scheduled(every = "${eclipse.git-eca.tasks.gh-revalidation.frequency:60s}")
    @ActivateRequestContext
    public void revalidate() {
        // if not enabled, don't process any potentially OOD records
        if (!Boolean.TRUE.equals(isEnabled.get())) {
            return;
        }

        // set up params for looking up the top of the revalidation queue
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(GitEcaParameterNames.NEEDS_REVALIDATION_RAW, "true");
        params.add(PersistenceUrlParameterNames.SORT.getName(), "lastUpdated");
        params.add(DefaultUrlParameterNames.PAGESIZE.getName(), "1");

        // build the request and query to lookup the longest standing request that needs revalidation
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("https://api.eclipse.org/git/eca/revalidation-queue"));
        RDBMSQuery<GithubWebhookTracking> trackingQuery = new RDBMSQuery<>(wrap, filters.get(GithubWebhookTracking.class), params);

        List<GithubWebhookTracking> oldestRevalidation = dao.get(trackingQuery);
        if (oldestRevalidation.isEmpty()) {
            LOGGER.debug("No queued revalidation requests found");
        } else {
            reprocessRequest(oldestRevalidation.get(0), wrap);
        }
    }

    /**
     * Reprocess the given record, attempting to run the ECA validation logic again. If it passes, the revalidation flag is
     * set to false and the time code is updated. If the processing fails again, the failure count gets incremented and the
     * updated time is set so that another entry can be updated, as to not block on potentially broken records.
     * 
     * @param requestToRevalidate the webhook tracking request to attempt to revalidate
     * @param wrap the current stubbed request wrapper used for queries.
     */
    private void reprocessRequest(GithubWebhookTracking requestToRevalidate, RequestWrapper wrap) {
        LOGGER
                .debug("Attempting revalidation of request w/ ID {}, in repo {}#{}", requestToRevalidate.getId(),
                        requestToRevalidate.getRepositoryFullName(), requestToRevalidate.getPullRequestNumber());

        // update the number of times this status has revalidated (tracking)
        requestToRevalidate
                .setManualRevalidationCount(requestToRevalidate.getManualRevalidationCount() == null ? 1
                        : requestToRevalidate.getManualRevalidationCount() + 1);
        // wrap in try-catch to avoid errors from stopping the record updates
        try {
            // split the full repo name into the org and repo name
            String[] repoFullNameParts = requestToRevalidate.getRepositoryFullName().split("/");
            if (repoFullNameParts.length != FULL_REPO_NAME_PARTS) {
                throw new IllegalStateException("Record with ID '" + Long.toString(requestToRevalidate.getId())
                        + "' is in an invalid state (repository full name is not valid)");
            }

            // run the validation and then check if it succeeded. Use the forced flag since we want to try even if there are no
            // changes
            validationHelper
                    .validateIncomingRequest(wrap, repoFullNameParts[0], repoFullNameParts[1], requestToRevalidate.getPullRequestNumber(),
                            true);
            // if we have gotten here, then the validation has completed and can be removed from queue
            requestToRevalidate.setNeedsRevalidation(false);
            LOGGER.debug("Sucessfully revalidated request w/ ID {}", requestToRevalidate.getId());
        } catch (RuntimeException e) {
            // log the message so we can see what happened
            LOGGER.error("Error while revalidating request w/ ID {}", requestToRevalidate.getId(), e);
        } finally {
            // whether successful or failed, update the updated time field
            requestToRevalidate.setLastUpdated(DateTimeHelper.now());

            // if in a closed state, we shouldn't try to revalidate again as it will never be valid
            // if a PR is reopened, it should create a new event and validate anyways
            if ("closed".equalsIgnoreCase(requestToRevalidate.getLastKnownState())) {
                LOGGER.debug("Tracking request {} set to not revalidate as it was closed", requestToRevalidate.getId());
                requestToRevalidate.setNeedsRevalidation(false);
            }
            // push the update with the potentially updated validation flag or error count
            dao.add(new RDBMSQuery<>(wrap, filters.get(GithubWebhookTracking.class)), Arrays.asList(requestToRevalidate));
        }
    }
}
