/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.tasks;

import java.net.URI;
import java.util.Date;
import java.util.List;

import org.apache.maven.shared.utils.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.git.eca.api.GithubAPI;
import org.eclipsefoundation.git.eca.api.models.GithubApplicationInstallationData;
import org.eclipsefoundation.git.eca.config.WebhooksConfig;
import org.eclipsefoundation.git.eca.dto.GithubApplicationInstallation;
import org.eclipsefoundation.git.eca.helper.JwtHelper;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.scheduler.Scheduled;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.control.ActivateRequestContext;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Using the configured database, Github installation records are tracked for all installations available for the
 * currently configured GH application. This is used over an in-memory cache as the calls to build the cache are long
 * running and have been known to fail sporadically.
 */
@ApplicationScoped
public class GithubInstallationUpdateTask {
    private static final Logger LOGGER = LoggerFactory.getLogger(GithubInstallationUpdateTask.class);

    @ConfigProperty(name = "eclipse.git-eca.tasks.gh-installation.enabled", defaultValue = "true")
    Instance<Boolean> isEnabled;
    @Inject
    WebhooksConfig config;

    @RestClient
    GithubAPI gh;

    @Inject
    JwtHelper jwt;
    @Inject
    APIMiddleware middle;
    @Inject
    PersistenceDao dao;
    @Inject
    FilterService filters;

    @PostConstruct
    void init() {
        // indicate to log whether enabled to reduce log spam
        LOGGER.info("Github installation DB cache task is{} enabled.", Boolean.TRUE.equals(isEnabled.get()) ? "" : " not");
    }

    /**
     * Every 1h, this method will query Github for installations for the currently configured application. These
     * installations will be translated into database entries for a persistent cache to be loaded on request.
     */
    @Scheduled(every = "1h", delay = 1)
    @ActivateRequestContext
    public void revalidate() {
        // if not enabled, don't process any potentially OOD records
        if (!Boolean.TRUE.equals(isEnabled.get())) {
            return;
        }

        // get the installations for the currently configured app
        List<GithubApplicationInstallationData> installations = middle
                .getAll(i -> gh.getInstallations(i, "Bearer " + jwt.generateJwt()), GithubApplicationInstallationData.class);
        // check that there are installations
        if (installations.isEmpty()) {
            LOGGER.warn("Did not find any installations for the currently configured Github application");
            return;
        }
        // trace log the installations for more context
        LOGGER.debug("Found {} installations to cache", installations.size());

        // create a common timestamp for easier lookups of stale entries
        Date startingTimestamp = new Date();
        // from installations, build records and start the processing for each entry
        List<GithubApplicationInstallation> installationRecords = installations
                .stream()
                .map(this::processInstallation)
                .toList();

        // once records are prepared, persist them back to the database with updates where necessary as a batch
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("https://api.eclipse.org/git/webhooks/github/installations"));
        List<GithubApplicationInstallation> repoRecords = dao
                .add(new RDBMSQuery<>(wrap, filters.get(GithubApplicationInstallation.class)), installationRecords);
        if (repoRecords.size() != installationRecords.size()) {
            LOGGER.warn("Background update to installation records had a size mismatch, cleaning will be skipped for this run");
            return;
        }

        // build query to do cleanup of stale records
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(GitEcaParameterNames.APPLICATION_ID_RAW, Integer.toString(config.github().appId()));
        params.add(GitEcaParameterNames.LAST_UPDATED_BEFORE_RAW, DateTimeHelper.toRFC3339(startingTimestamp));

        // run the delete call, removing stale entries
        dao.delete(new RDBMSQuery<>(wrap, filters.get(GithubApplicationInstallation.class), params));
    }

    /**
     * Converts the raw installation data from Github into a short record to be persisted to database as a form of
     * persistent caching. Checks database for existing record, and returns record with a touched date for existing entries.
     * 
     * @param ghInstallation raw Github installation record for current application
     * @return the new or updated installation record to be persisted to the database.
     */
    private GithubApplicationInstallation processInstallation(GithubApplicationInstallationData ghInstallation) {
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("https://api.eclipse.org/git/webhooks/github/installations"));
        // build the lookup query for the current installation record
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(GitEcaParameterNames.APPLICATION_ID_RAW, Integer.toString(config.github().appId()));
        params.add(GitEcaParameterNames.INSTALLATION_ID_RAW, Integer.toString(ghInstallation.getId()));

        // lookup existing records in the database
        List<GithubApplicationInstallation> existingRecords = dao
                .get(new RDBMSQuery<>(wrap, filters.get(GithubApplicationInstallation.class), params));

        // check for existing entry, creating if missing
        GithubApplicationInstallation installation;
        if (existingRecords == null || existingRecords.isEmpty()) {
            installation = new GithubApplicationInstallation();
            installation.setAppId(config.github().appId());
            installation.setInstallationId(ghInstallation.getId());
        } else {
            installation = existingRecords.get(0);
        }
        // update the basic stats to handle renames, and update last updated time
        // login is technically nullable, so this might be missing. This is best we can do, as we can't look up by id
        installation
                .setName(StringUtils.isNotBlank(ghInstallation.getAccount().getLogin()) ? ghInstallation.getAccount().getLogin()
                        : "UNKNOWN");
        installation.setLastUpdated(new Date());
        return installation;

    }
}
