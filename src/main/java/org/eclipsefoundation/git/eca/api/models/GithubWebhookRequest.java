/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.api.models;

import jakarta.annotation.Nullable;

import org.eclipsefoundation.git.eca.dto.GithubWebhookTracking;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Represents incoming webhook requests from the Git ECA app installations on Github.
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_GithubWebhookRequest.Builder.class)
public abstract class GithubWebhookRequest {

    public abstract Installation getInstallation();

    public abstract Repository getRepository();

    @Nullable
    public abstract PullRequest getPullRequest();

    /**
     * Generate basic builder with default properties for constructing a webhook request.
     * 
     * @return the default builder for constructing a webhook request model.
     */
    public static Builder builder() {
        return new AutoValue_GithubWebhookRequest.Builder();
    }

    /**
     * Reconstructs a webhook request from the tracked webhook data.
     * 
     * @param tracking the tracked webhook request data to use in reconstructing the request.
     * @return the reconstructed request.
     */
    public static GithubWebhookRequest buildFromTracking(GithubWebhookTracking tracking) {
        return builder()
                .setInstallation(Installation.builder().setId(tracking.getInstallationId()).build())
                .setPullRequest(PullRequest
                        .builder()
                        .setNumber(tracking.getPullRequestNumber())
                        .setHead(PullRequestHead.builder().setSha(tracking.getHeadSha()).build())
                        .setState(tracking.getLastKnownState())
                        .build())
                .setRepository(Repository
                        .builder()
                        .setFullName(tracking.getRepositoryFullName())
                        .setHtmlUrl("https://github.com/" + tracking.getRepositoryFullName())
                        .build())
                .build();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setInstallation(Installation installation);

        public abstract Builder setRepository(Repository repository);

        public abstract Builder setPullRequest(@Nullable PullRequest pullRequest);

        public abstract GithubWebhookRequest build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_GithubWebhookRequest_Repository.Builder.class)
    public abstract static class Repository {

        public abstract String getFullName();

        public abstract String getHtmlUrl();

        public static Builder builder() {
            return new AutoValue_GithubWebhookRequest_Repository.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setFullName(String fullName);

            public abstract Builder setHtmlUrl(String htmlUrl);

            public abstract Repository build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_GithubWebhookRequest_PullRequest.Builder.class)
    public abstract static class PullRequest {

        public abstract String getState();

        public abstract Integer getNumber();

        public abstract PullRequestHead getHead();

        public static Builder builder() {
            return new AutoValue_GithubWebhookRequest_PullRequest.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setState(String state);

            public abstract Builder setNumber(Integer number);

            public abstract Builder setHead(PullRequestHead head);

            public abstract PullRequest build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_GithubWebhookRequest_PullRequestHead.Builder.class)
    public abstract static class PullRequestHead {

        public abstract String getSha();

        public static Builder builder() {
            return new AutoValue_GithubWebhookRequest_PullRequestHead.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setSha(String sha);

            public abstract PullRequestHead build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_GithubWebhookRequest_Installation.Builder.class)
    public abstract static class Installation {
        public abstract String getId();

        public static Builder builder() {
            return new AutoValue_GithubWebhookRequest_Installation.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setId(String id);

            public abstract Installation build();
        }
    }
}
