/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.api.models;

import jakarta.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Information about the given Github ECA application installation
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_GithubApplicationInstallationData.Builder.class)
public abstract class GithubApplicationInstallationData {

    public abstract int getId();

    public abstract String getTargetType();

    public abstract String getTargetId();

    public abstract Account getAccount();

    public static Builder builder() {
        return new AutoValue_GithubApplicationInstallationData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setId(int id);

        public abstract Builder setTargetType(String targetType);

        public abstract Builder setTargetId(String targetId);

        public abstract Builder setAccount(Account account);

        public abstract GithubApplicationInstallationData build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_GithubApplicationInstallationData_Account.Builder.class)
    public abstract static class Account {
        public abstract int getId();

        @Nullable
        public abstract String getLogin();

        public static Builder builder() {
            return new AutoValue_GithubApplicationInstallationData_Account.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setId(int id);

            public abstract Builder setLogin(@Nullable String login);

            public abstract Account build();
        }
    }
}
