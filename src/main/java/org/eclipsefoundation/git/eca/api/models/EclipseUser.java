/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.api.models;

import jakarta.annotation.Nullable;

import org.eclipsefoundation.git.eca.model.GitUser;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Represents a users Eclipse Foundation account
 *
 * @author Martin Lowe
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_EclipseUser.Builder.class)
public abstract class EclipseUser {
    public abstract int getUid();

    public abstract String getName();

    public abstract String getMail();

    public abstract ECA getECA();

    public abstract boolean getIsCommitter();

    @Nullable
    public abstract String getGithubHandle();

    @Nullable
    @JsonIgnore
    public abstract Boolean getIsBot();

    /**
     * Create a bot user stub when there is no real Eclipse account for the bot.
     *
     * @param user the Git user that was detected to be a bot.
     * @return a stubbed Eclipse user bot object.
     */
    public static EclipseUser createBotStub(GitUser user) {
        return EclipseUser
                .builder()
                .setUid(0)
                .setName(user.getName())
                .setMail(user.getMail())
                .setECA(ECA.builder().build())
                .setIsBot(true)
                .build();
    }

    public static Builder builder() {
        return new AutoValue_EclipseUser.Builder().setIsCommitter(false).setIsBot(false);
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setUid(int id);

        public abstract Builder setName(String name);

        public abstract Builder setMail(String mail);

        public abstract Builder setECA(ECA eca);

        public abstract Builder setIsCommitter(boolean isCommitter);

        public abstract Builder setGithubHandle(@Nullable String githubHandle);

        @JsonIgnore
        public abstract Builder setIsBot(@Nullable Boolean isBot);

        public abstract EclipseUser build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_EclipseUser_ECA.Builder.class)
    public abstract static class ECA {
        public abstract boolean getSigned();

        public abstract boolean getCanContributeSpecProject();

        public static Builder builder() {
            return new AutoValue_EclipseUser_ECA.Builder().setCanContributeSpecProject(false).setSigned(false);
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setSigned(boolean signed);

            public abstract Builder setCanContributeSpecProject(boolean canContributeSpecProject);

            public abstract ECA build();
        }
    }
}
