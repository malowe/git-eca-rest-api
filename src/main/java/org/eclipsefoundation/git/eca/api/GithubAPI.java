/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.api;

import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.git.eca.api.models.GithubAccessToken;
import org.eclipsefoundation.git.eca.api.models.GithubCommitStatusRequest;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.PullRequest;
import org.jboss.resteasy.util.HttpHeaderNames;

/**
 * Bindings used by the webhook logic to retrieve data about the PR to be validated.
 * 
 * @author Martin Lowe
 *
 */
@RegisterRestClient(baseUri = "https://api.github.com")
@Produces("application/json")
public interface GithubAPI {

    /**
     * Retrieves information about a certain pull request in a repo if it exists
     * 
     * @param bearer authorization header value, access token for application with access to repo
     * @param apiVersion the version of the GH API to target when making the request
     * @param repoFull the full repo name that is being targeted
     * @param pullNumber the pull request number
     * @return information about the given pull request if it exists.
     */
    @GET
    @Path("repos/{repoFull}/pulls/{pullNumber}")
    public PullRequest getPullRequest(@HeaderParam(HttpHeaderNames.AUTHORIZATION) String bearer,
            @HeaderParam("X-GitHub-Api-Version") String apiVersion, @PathParam("repoFull") String repoFull,
            @PathParam("pullNumber") int pullNumber);

    /**
     * Retrieves a list of commits related to the given pull request.
     * 
     * @param bearer authorization header value, access token for application with access to repo
     * @param apiVersion the version of the GH API to target when making the request
     * @param repoFull the full repo name that is being targeted
     * @param pullNumber the pull request number
     * @return list of commits associated with the pull request, wrapped in a jax-rs response
     */
    @GET
    @Path("repos/{repoFull}/pulls/{pullNumber}/commits")
    public Response getCommits(@HeaderParam(HttpHeaderNames.AUTHORIZATION) String bearer,
            @HeaderParam("X-GitHub-Api-Version") String apiVersion, @PathParam("repoFull") String repoFull,
            @PathParam("pullNumber") int pullNumber);

    /**
     * Posts an update to the Github API, using an access token to update a given pull requests commit status, targeted
     * using the head sha.
     * 
     * @param bearer authorization header value, access token for application with access to repo
     * @param apiVersion the version of the GH API to target when making the request
     * @param repoFull the full repo name that is being targeted
     * @param prHeadSha the head SHA for the request
     * @param commitStatusUpdate the status body sent with the request
     * @return JAX-RS response to check for success or failure based on status code.
     */
    @POST
    @Path("repos/{repoFull}/statuses/{prHeadSha}")
    public Response updateStatus(@HeaderParam(HttpHeaderNames.AUTHORIZATION) String bearer,
            @HeaderParam("X-GitHub-Api-Version") String apiVersion, @PathParam("repoFull") String repoFull,
            @PathParam("prHeadSha") String prHeadSha, GithubCommitStatusRequest commitStatusUpdate);

    /**
     * Requires a JWT bearer token for the application to retrieve installations for. Returns a list of installations for
     * the given application.
     * 
     * @param params the general params for requests, including pagination
     * @param bearer JWT bearer token for the target application
     * @return list of installations for the application
     */
    @GET
    @Path("app/installations")
    public Response getInstallations(@BeanParam BaseAPIParameters params, @HeaderParam(HttpHeaderNames.AUTHORIZATION) String bearer);

    /**
     * Retrieves an access token for a specific installation, given the applications JWT bearer and the api version.
     * 
     * @param bearer the authorization header value, should contain the apps signed JWT token
     * @param apiVersion the API version to target with the request
     * @param installationId the installation to generate an access token for
     * @return the Github access token for the GH app installation
     */
    @POST
    @Path("app/installations/{installationId}/access_tokens")
    public GithubAccessToken getNewAccessToken(@HeaderParam(HttpHeaderNames.AUTHORIZATION) String bearer,
            @HeaderParam("X-GitHub-Api-Version") String apiVersion, @PathParam("installationId") String installationId);

    /**
     * Returns a list of repositories for the given installation.
     * 
     * @param params the general params for requests, including pagination
     * @param bearer JWT bearer token for the target installation
     * @return list of repositories for the installation as a response for pagination
     */
    @GET
    @Path("installation/repositories")
    public Response getInstallationRepositories(@BeanParam BaseAPIParameters params,
            @HeaderParam(HttpHeaderNames.AUTHORIZATION) String bearer);

}
