/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.api;

import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Interface for interacting with the Eclipse Foundation Bots API.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
@Path("/bots")
@RegisterRestClient
public interface BotsAPI {

	/**
	 * Retrieves all bot users from the endpoint.
	 * 
	 * @return list of active bot users
	 */
	@GET
	@Produces("application/json")
	List<JsonNode> getBots();
}
