/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.config;

import java.util.List;

import io.smallrye.config.ConfigMapping;

/**
 * Captures all configs related to mail validation. Includesan allow list and a list of no-reply email patterns.
 */
@ConfigMapping(prefix = "eclipse.git-eca.mail-validation")
public interface MailValidationConfig {
    
    public List<String> allowList();

    public List<String> noreplyEmailPatterns();
}
