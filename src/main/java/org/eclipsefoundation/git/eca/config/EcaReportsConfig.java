/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.config;

import java.util.List;

import io.smallrye.config.ConfigMapping;

/**
 * Captures all configs related to eca reports. Includes resource access key and list of allowed users.
 */
@ConfigMapping(prefix = "eclipse.git-eca.reports")
public interface EcaReportsConfig {

    String accessKey();

    List<String> allowedUsers();
}
