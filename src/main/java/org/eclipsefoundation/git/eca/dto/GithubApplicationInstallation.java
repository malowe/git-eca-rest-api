/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.dto;

import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

/**
 * Tracking for github installation data for GH ECA applications used for requesting data and updating commit statuses.
 */
@Table
@Entity
public class GithubApplicationInstallation extends BareNode {
    public static final DtoTable TABLE = new DtoTable(GithubApplicationInstallation.class, "gai");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int appId;
    private int installationId;
    private String name;
    private Date lastUpdated;

    @Override
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the appId
     */
    public int getAppId() {
        return appId;
    }

    /**
     * @param appId the appId to set
     */
    public void setAppId(int appId) {
        this.appId = appId;
    }

    /**
     * @return the installationId
     */
    public int getInstallationId() {
        return installationId;
    }

    /**
     * @param installationId the installationId to set
     */
    public void setInstallationId(int installationId) {
        this.installationId = installationId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the lastUpdated
     */
    public Date getLastUpdated() {
        return lastUpdated;
    }

    /**
     * @param lastUpdated the lastUpdated to set
     */
    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(appId, id, installationId, lastUpdated, name);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GithubApplicationInstallation other = (GithubApplicationInstallation) obj;
        return appId == other.appId && id == other.id && installationId == other.installationId
                && Objects.equals(lastUpdated, other.lastUpdated) && Objects.equals(name, other.name);
    }

    @Singleton
    public static class GithubApplicationInstallationFilter implements DtoFilter<GithubApplicationInstallation> {

        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);

            String installationId = params.getFirst(GitEcaParameterNames.INSTALLATION_ID_RAW);
            if (StringUtils.isNotBlank(installationId)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".installationId = ?",
                                new Object[] { Integer.parseInt(installationId) }));
            }
            String appId = params.getFirst(GitEcaParameterNames.APPLICATION_ID_RAW);
            if (StringUtils.isNumeric(appId)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".appId = ?",
                                new Object[] { Integer.parseInt(appId) }));
            }
            String lastUpdated = params.getFirst(GitEcaParameterNames.LAST_UPDATED_BEFORE_RAW);
            if (StringUtils.isNotBlank(lastUpdated)) {
                try {
                    Date dt = DateTimeHelper.toRFC3339(lastUpdated);
                    statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".lastUpdated < ?", new Object[] { dt }));
                } catch (DateTimeParseException e) {
                    // do not do anything for badly formatted args
                }
            }
            return statement;
        }

        @Override
        public Class<GithubApplicationInstallation> getType() {
            return GithubApplicationInstallation.class;
        }
    }
}
