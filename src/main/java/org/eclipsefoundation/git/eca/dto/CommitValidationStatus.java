/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement.ComplexJoin;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement.ComplexJoin.JoinPart;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

@Table
@Entity
public class CommitValidationStatus extends BareNode implements Serializable {
    public static final DtoTable TABLE = new DtoTable(CommitValidationStatus.class, "cvs");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String commitHash;
    private String userMail;
    private String project;
    private String repoUrl;
    @Enumerated(EnumType.STRING)
    private ProviderType provider;
    private Integer estimatedLoc;
    private ZonedDateTime creationDate;
    private ZonedDateTime lastModified;
    @OneToMany(mappedBy = "commit", fetch = FetchType.EAGER, cascade = { CascadeType.ALL }, orphanRemoval = true)
    private List<CommitValidationMessage> errors;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the sha
     */
    public String getCommitHash() {
        return commitHash;
    }

    /**
     * @param commitHash the sha to set
     */
    public void setCommitHash(String commitHash) {
        this.commitHash = commitHash;
    }

    /**
     * @return the userMail
     */
    public String getUserMail() {
        return userMail;
    }

    /**
     * @param userMail the userMail to set
     */
    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    /**
     * @return the project
     */
    public String getProject() {
        return project;
    }

    /**
     * @param project the project to set
     */
    public void setProject(String project) {
        this.project = project;
    }

    /**
     * @return the repoUrl
     */
    public String getRepoUrl() {
        return repoUrl;
    }

    /**
     * @param repoUrl the repoUrl to set
     */
    public void setRepoUrl(String repoUrl) {
        this.repoUrl = repoUrl;
    }

    /**
     * @return the provider
     */
    public ProviderType getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(ProviderType provider) {
        this.provider = provider;
    }

    /**
     * @return the estimatedLoc
     */
    public Integer getEstimatedLoc() {
        return estimatedLoc;
    }

    /**
     * @param estimatedLoc the estimatedLoc to set
     */
    public void setEstimatedLoc(Integer estimatedLoc) {
        this.estimatedLoc = estimatedLoc;
    }

    /**
     * @return the creationDate
     */
    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the lastModified
     */
    public ZonedDateTime getLastModified() {
        return lastModified;
    }

    /**
     * @param lastModified the lastModified to set
     */
    public void setLastModified(ZonedDateTime lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * @return the messages
     */
    public List<CommitValidationMessage> getErrors() {
        return errors;
    }

    /**
     * @param errors the errors to set
     */
    public void setErrors(List<CommitValidationMessage> errors) {
        this.errors = errors;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + Objects.hash(commitHash, creationDate, errors, estimatedLoc, id, lastModified, project, provider, repoUrl, userMail);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CommitValidationStatus other = (CommitValidationStatus) obj;
        return Objects.equals(commitHash, other.commitHash) && Objects.equals(creationDate, other.creationDate)
                && Objects.equals(errors, other.errors) && Objects.equals(estimatedLoc, other.estimatedLoc) && Objects.equals(id, other.id)
                && Objects.equals(lastModified, other.lastModified) && Objects.equals(project, other.project) && provider == other.provider
                && Objects.equals(repoUrl, other.repoUrl) && Objects.equals(userMail, other.userMail);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CommitValidationStatus [id=");
        builder.append(id);
        builder.append(", sha=");
        builder.append(commitHash);
        builder.append(", userMail=");
        builder.append(userMail);
        builder.append(", project=");
        builder.append(project);
        builder.append(", repoUrl=");
        builder.append(repoUrl);
        builder.append(", provider=");
        builder.append(provider);
        builder.append(", estimatedLoc=");
        builder.append(estimatedLoc);
        builder.append(", creationDate=");
        builder.append(creationDate);
        builder.append(", lastModified=");
        builder.append(lastModified);
        builder.append(", messages=");
        builder.append(errors.toString());
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class CommitValidationStatusFilter implements DtoFilter<CommitValidationStatus> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            // sha check
            String commitHash = params.getFirst(GitEcaParameterNames.SHA.getName());
            if (StringUtils.isNumeric(commitHash)) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".commitHash = ?", new Object[] { commitHash }));
            }
            String projectId = params.getFirst(GitEcaParameterNames.PROJECT_ID.getName());
            if (StringUtils.isNumeric(projectId)) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".projectId = ?", new Object[] { projectId }));
            }
            List<String> commitHashes = params.get(GitEcaParameterNames.SHAS.getName());
            if (commitHashes != null && !commitHashes.isEmpty()) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".commitHash IN ?", new Object[] { commitHashes }));
            }
            String repoUrl = params.getFirst(GitEcaParameterNames.REPO_URL.getName());
            if (StringUtils.isNotBlank(repoUrl)) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".repoUrl = ?", new Object[] { repoUrl }));
            }
            String userMail = params.getFirst(GitEcaParameterNames.USER_MAIL.getName());
            if (StringUtils.isNotBlank(userMail)) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".userMail = ?", new Object[] { userMail }));
            }
            String fingerprint = params.getFirst(GitEcaParameterNames.FINGERPRINT.getName());
            if (StringUtils.isNotBlank(fingerprint)) {
                stmt.addJoin(new ComplexJoin(TABLE, CommitValidationStatusGrouping.TABLE, new JoinPart("id", "compositeId.commit.id")));
                stmt
                        .addClause(new ParameterizedSQLStatement.Clause(
                                CommitValidationStatusGrouping.TABLE.getAlias() + ".compositeId.fingerprint = ?",
                                new Object[] { fingerprint }));
            }

            return stmt;
        }

        @Override
        public Class<CommitValidationStatus> getType() {
            return CommitValidationStatus.class;
        }
    }
}
