/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.helper;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.git.eca.api.HCaptchaCallbackAPI;
import org.eclipsefoundation.git.eca.config.HcaptchaConfig;
import org.eclipsefoundation.git.eca.namespace.HCaptchaErrorCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper for validating captcha responses.
 * 
 * @author Martin Lowe
 *
 */
@Singleton
public class CaptchaHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(CaptchaHelper.class);

    @Inject
    HcaptchaConfig config;

    @RestClient
    HCaptchaCallbackAPI hcaptchaApi;

    /**
     * Validates a captcha response through the designated captcha server.
     * 
     * @param response the challenege response for a request
     * @return a list of errors if any occurred, or an empty list for a good request.
     */
    public List<HCaptchaErrorCodes> validateCaptchaResponse(String response) {
        // allow for disabling the captcha in case there are service issues, or in testing
        if (!config.enabled()) {
            LOGGER.trace("Captcha validation is currently disabled, skipping captcha check");
            return Collections.emptyList();
        } else if (StringUtils.isBlank(response)) {
            LOGGER.trace("Cannot process empty response, returning early");
            return Arrays.asList(HCaptchaErrorCodes.MISSING_INPUT_RESPONSE);
        }
        // validate the hcaptcha response with the site API, passing the secret and response
        return Optional
                .ofNullable(
                        hcaptchaApi.validateCaptchaRequest(response, config.secret(), config.siteKey()).getErrorCodes())
                .orElse(Collections.emptyList());
    }
}
