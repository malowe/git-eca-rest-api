/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.helper;

import java.net.URI;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.efservices.api.models.Project.GithubProject;
import org.eclipsefoundation.efservices.api.models.Project.ProjectParticipant;
import org.eclipsefoundation.efservices.services.ProjectService;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.namespace.ProviderType;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 * Helps manage projects by providing filters on top of the generic service as well as operations like adapting interest
 * groups to projects for easier processing.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public final class ProjectHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectHelper.class);

    @Inject
    CachingService cache;
    @Inject
    ProjectService projects;

    public List<Project> retrieveProjectsForRequest(ValidationRequest req) {
        String repoUrl = req.getRepoUrl().getPath();
        if (repoUrl == null) {
            LOGGER.warn("Can not match null repo URL to projects");
            return Collections.emptyList();
        }
        return retrieveProjectsForRepoURL(repoUrl, req.getProvider());
    }

    public List<Project> retrieveProjectsForRepoURL(String repoUrl, ProviderType provider) {
        if (repoUrl == null) {
            LOGGER.warn("Can not match null repo URL to projects");
            return Collections.emptyList();
        }
        // check for all projects that make use of the given repo
        List<Project> availableProjects = getProjects();
        if (availableProjects.isEmpty()) {
            LOGGER.warn("Could not find any projects to match against");
            return Collections.emptyList();
        }
        LOGGER.debug("Checking projects for repos that end with: {}", repoUrl);

        String projectNamespace = URI.create(repoUrl).getPath().substring(1).toLowerCase();
        // filter the projects based on the repo URL. At least one repo in project must
        // match the repo URL to be valid
        switch (provider) {
            case GITLAB:
                return availableProjects
                        .stream()
                        .filter(p -> projectNamespace.startsWith(p.getGitlab().getProjectGroup() + "/")
                                && p.getGitlab().getIgnoredSubGroups().stream().noneMatch(sg -> projectNamespace.startsWith(sg + "/")))
                        .toList();
            case GITHUB:
                return availableProjects
                        .stream()
                        .filter(p -> p.getGithubRepos().stream().anyMatch(re -> re.getUrl() != null && re.getUrl().endsWith(repoUrl))
                                || (StringUtils.isNotBlank(p.getGithub().getOrg()) && projectNamespace.startsWith(p.getGithub().getOrg())
                                        && p.getGithub().getIgnoredRepos().stream().noneMatch(repoUrl::endsWith)))
                        .toList();
            case GERRIT:
                return availableProjects
                        .stream()
                        .filter(p -> p.getGerritRepos().stream().anyMatch(re -> re.getUrl() != null && re.getUrl().endsWith(repoUrl)))
                        .toList();
            default:
                return Collections.emptyList();
        }
    }

    /**
     * Retrieve cached and adapted projects list to avoid having to create all interest group adaptations on every request.
     * 
     * @return list of available projects or empty list if none found.
     */
    public List<Project> getProjects() {
        return cache.get("all-combined", new MultivaluedMapImpl<>(), Project.class, () -> {
            List<Project> availableProjects = projects.getAllProjects();
            availableProjects.addAll(adaptInterestGroups(projects.getAllInterestGroups()));
            return availableProjects;
        }).getData().orElseGet(Collections::emptyList);
    }

    private List<Project> adaptInterestGroups(List<InterestGroup> igs) {
        return igs.stream().map(this::convertToProject).toList();
    }

    /**
     * Convert an interest group into a project, as they are essentially the same structure behind the API.
     * 
     * @param ig the interest group to convert
     * @return the project built from the interest group
     */
    private Project convertToProject(InterestGroup ig) {
        return Project
                .builder()
                .setProjectId(ig.getProjectId())
                .setGerritRepos(Collections.emptyList())
                .setGithubRepos(Collections.emptyList())
                .setGitlab(ig.getGitlab())
                .setCommitters(ig
                        .getParticipants()
                        .stream()
                        .map(p -> ProjectParticipant
                                .builder()
                                .setFullName(p.getFullName())
                                .setUrl(p.getUrl())
                                .setUsername(p.getUsername())
                                .build())
                        .toList())
                .setProjectLeads(ig
                        .getLeads()
                        .stream()
                        .map(p -> ProjectParticipant
                                .builder()
                                .setFullName(p.getFullName())
                                .setUrl(p.getUrl())
                                .setUsername(p.getUsername())
                                .build())
                        .toList())
                .setContributors(Collections.emptyList())
                .setShortProjectId(ig.getShortProjectId())
                .setSummary("")
                .setWebsiteUrl("")
                .setWebsiteRepo(Collections.emptyList())
                .setGitlab(ig.getGitlab())
                .setGithub(GithubProject.builder().setOrg("").setIgnoredRepos(Collections.emptyList()).build())
                .setWorkingGroups(Collections.emptyList())
                .setIndustryCollaborations(Collections.emptyList())
                .setReleases(Collections.emptyList())
                .setTopLevelProject("")
                .setUrl("")
                .setLogo(ig.getLogo())
                .setTags(Collections.emptyList())
                .setName(ig.getTitle())
                .setSpecProjectWorkingGroup(Collections.emptyMap())
                .build();
    }
}
