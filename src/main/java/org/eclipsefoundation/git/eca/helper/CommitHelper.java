/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.helper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.eclipsefoundation.core.exception.ApplicationException;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

import io.undertow.util.HexConverter;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Contains helpers for processing commits.
 *
 * @author Martin Lowe
 *
 */
public class CommitHelper {

    /**
     * Validate the commits fields.
     *
     * @param c commit to validate
     * @return true if valid, otherwise false
     */
    public static boolean validateCommit(Commit c) {
        if (c == null) {
            return false;
        }

        boolean valid = true;
        // check current commit data
        if (c.getHash() == null) {
            valid = false;
        }
        // check author
        if (c.getAuthor() == null || c.getAuthor().getMail() == null) {
            valid = false;
        }
        // check committer
        if (c.getCommitter() == null || c.getCommitter().getMail() == null) {
            valid = false;
        }

        return valid;
    }

    public static MultivaluedMap<String, String> getCommitParams(ValidationRequest req, String projectId) {
        return getCommitParams(req.getCommits().stream().map(Commit::getHash).toList(), projectId,
                req.getRepoUrl().toString());
    }

    public static MultivaluedMap<String, String> getCommitParams(List<String> commitShas, String projectId, String repoUrl) {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.put(GitEcaParameterNames.SHAS_RAW, commitShas);
        params.add(GitEcaParameterNames.REPO_URL_RAW, repoUrl);
        if (projectId != null) {
            params.add(GitEcaParameterNames.PROJECT_ID_RAW, projectId);
        }
        return params;
    }

    /**
     * Generates a request fingerprint for looking up requests that have already been processed in the past. Collision here
     * is extremely unlikely, and low risk on the change it does. For that reason, a more secure but heavier hashing alg.
     * wasn't chosen.
     * 
     * @param req the request to generate a fingerprint for
     * @return the fingerprint for the request
     */
    public static String generateRequestHash(ValidationRequest req) {
        StringBuilder sb = new StringBuilder();
        sb.append(req.getRepoUrl());
        req.getCommits().forEach(c -> sb.append(c.getHash()));
        try {
            return HexConverter.convertToHexString(MessageDigest.getInstance("MD5").digest(sb.toString().getBytes()));
        } catch (NoSuchAlgorithmException e) {
            throw new ApplicationException("Error while encoding request fingerprint - couldn't find MD5 algorithm.", e);
        }
    }

    /**
     * Centralized way of retrieving a checked project ID from a project for use when interacting with commits and commit
     * data.
     * 
     * @param p the current project to attempt to retrieve an ID from
     * @return the project ID or the given default (empty string).
     */
    public static String getProjectId(Project p) {
        return p != null ? p.getProjectId() : "";
    }

    private CommitHelper() {
    }
}
