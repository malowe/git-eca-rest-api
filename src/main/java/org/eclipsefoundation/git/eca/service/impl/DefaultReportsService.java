/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service.impl;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.git.eca.dto.PrivateProjectEvent;
import org.eclipsefoundation.git.eca.model.PrivateProjectData;
import org.eclipsefoundation.git.eca.model.mappers.PrivateProjectEventMapper;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.service.ReportsService;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

@ApplicationScoped
public class DefaultReportsService implements ReportsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultReportsService.class);

    @Inject
    PersistenceDao dao;
    @Inject
    FilterService filters;

    @Inject
    PrivateProjectEventMapper mapper;

    @Override
    public List<PrivateProjectData> getPrivateProjectEvents(RequestWrapper wrap, String status, LocalDate since,
            LocalDate until) {

        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();

        if (StringUtils.isNotBlank(status)) {
            if (status.equalsIgnoreCase(GitEcaParameterNames.STATUS_ACTIVE.getName())) {
                params.add(GitEcaParameterNames.STATUS_ACTIVE.getName(), status);
            } else {
                params.add(GitEcaParameterNames.STATUS_DELETED.getName(), status);
            }
        }
        if (since != null) {
            params.add(GitEcaParameterNames.SINCE.getName(), since.toString());
        }
        if (until != null) {
            params.add(GitEcaParameterNames.UNTIL.getName(), until.toString());
        }

        List<PrivateProjectEvent> results = dao
                .get(new RDBMSQuery<>(wrap, filters.get(PrivateProjectEvent.class), params));
        if (results == null) {
            LOGGER.error("Error retrieving PrivateProjectEvents from the DB using params: {}", params);
            return Collections.emptyList();
        }

        // Map to models
        return results.stream().map(mapper::toModel).toList();
    }
}
