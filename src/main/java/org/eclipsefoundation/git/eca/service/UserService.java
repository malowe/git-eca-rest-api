/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service;

import java.util.List;

import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.git.eca.api.models.EclipseUser;

public interface UserService {

    /**
     * Retrieves an Eclipse Account user object given the Git users email address
     * (at minimum). This is facilitated using the Eclipse Foundation accounts API,
     * along short lived in-memory caching for performance and some protection
     * against duplicate requests.
     *
     * @param mail the email address to use to retrieve the Eclipse account for.
     * @return the Eclipse Account user information if found, or null if there was
     *         an error or no user exists.
     */
    EclipseUser getUser(String mail);

    /**
     * Retrieves an Eclipse Account user object given the Github username. This is
     * facilitated using the Eclipse Foundation accounts API, along short lived
     * in-memory caching for performance and some protection against duplicate
     * requests.
     *
     * @param username the Github username used for retrieval of associated Eclipse
     *                 Account if it exists.
     * @return the Eclipse Account user information if found, or null if there was
     *         an error or no user exists.
     */
    EclipseUser getUserByGithubUsername(String username);

    /**
     * Checks the bot API to see whether passed email address is registered to a bot
     * under the passed projects.
     * 
     * @param mail             the potential bot user's email address
     * @param filteredProjects the projects to check for bot presence.
     * @return true if the user is a bot on at least one of the given projects,
     *         false otherwise.
     */
    boolean userIsABot(String mail, List<Project> filteredProjects);
}
