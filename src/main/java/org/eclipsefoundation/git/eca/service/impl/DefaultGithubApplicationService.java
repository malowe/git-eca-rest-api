/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.git.eca.service.impl;

import java.net.URI;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.exception.ApplicationException;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.git.eca.api.GithubAPI;
import org.eclipsefoundation.git.eca.api.models.GithubWebhookRequest.PullRequest;
import org.eclipsefoundation.git.eca.config.WebhooksConfig;
import org.eclipsefoundation.git.eca.dto.GithubApplicationInstallation;
import org.eclipsefoundation.git.eca.helper.JwtHelper;
import org.eclipsefoundation.git.eca.namespace.GitEcaParameterNames;
import org.eclipsefoundation.git.eca.service.GithubApplicationService;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.Caffeine;

/**
 * Default caching implementation of the GH app service. This uses a loading cache to keep installation info highly
 * available to reduce latency in calls.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class DefaultGithubApplicationService implements GithubApplicationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultGithubApplicationService.class);

    // cache kept small as there should only ever really be 1 entry, but room added for safety
    private static final Integer MAX_CACHE_SIZE = 10;
    private static final Long INSTALL_REPO_FETCH_MAX_TIMEOUT = 15l;

    @ConfigProperty(name = "eclipse.github.default-api-version", defaultValue = "2022-11-28")
    String apiVersion;
    @Inject
    WebhooksConfig config;

    @RestClient
    GithubAPI gh;

    @Inject
    PersistenceDao dao;
    @Inject
    FilterService filters;
    @Inject
    CachingService cache;
    @Inject
    APIMiddleware middle;

    @Inject
    JwtHelper jwt;
    @Inject
    ManagedExecutor exec;

    private AsyncLoadingCache<String, List<GithubApplicationInstallation>> installationRepositoriesCache;

    @PostConstruct
    void init() {
        this.installationRepositoriesCache = Caffeine
                .newBuilder()
                .executor(exec)
                .maximumSize(MAX_CACHE_SIZE)
                .refreshAfterWrite(Duration.ofMinutes(TimeUnit.MINUTES.convert(1, TimeUnit.HOURS)))
                .buildAsync(k -> loadInstallationRepositories());
        // do initial map population
        getAllInstallRepos();
    }

    @Override
    public List<GithubApplicationInstallation> getManagedInstallations() {
        return new ArrayList<>(getAllInstallRepos());
    }

    @Override
    public String getInstallationForRepo(String org, String repo) {
        return getAllInstallRepos()
                .stream()
                .filter(install -> install.getName().equalsIgnoreCase(org))
                .map(install -> Integer.toString(install.getInstallationId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Optional<PullRequest> getPullRequest(String installationId, String repoFullName, Integer pullRequest) {
        return cache
                .get(repoFullName, new MultivaluedMapImpl<>(), PullRequest.class,
                        () -> gh.getPullRequest(jwt.getGhBearerString(installationId), apiVersion, repoFullName, pullRequest))
                .getData();
    }

    private List<GithubApplicationInstallation> getAllInstallRepos() {
        try {
            return this.installationRepositoriesCache.get("all").get(INSTALL_REPO_FETCH_MAX_TIMEOUT, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.error("Thread interrupted while building repository cache, no entries will be available for current call");
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            // rewrap exception and throw
            throw new ApplicationException(
                    "Thread interrupted while building repository cache, no entries will be available for current call", e);
        }
        return Collections.emptyList();
    }

    /**
     * Retrieves a fresh copy of tracked installations, which have the associated app, the installation ID, and the name of
     * the namespace associated with it.
     * 
     * @return a multivalued map relating installation IDs to associated full repo names.
     */
    private List<GithubApplicationInstallation> loadInstallationRepositories() {
        // once records are prepared, persist them back to the database with updates where necessary as a batch
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("https://api.eclipse.org/git/webhooks/github/installations"));

        // build query to do fetch of records for currently active application
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(GitEcaParameterNames.APPLICATION_ID_RAW, Integer.toString(config.github().appId()));

        // we don't want to use the limit, as we want to pull all of the records we can find
        RDBMSQuery<GithubApplicationInstallation> q = new RDBMSQuery<>(wrap, filters.get(GithubApplicationInstallation.class), params);
        q.setUseLimit(false);
        return dao.get(q);
    }

}
