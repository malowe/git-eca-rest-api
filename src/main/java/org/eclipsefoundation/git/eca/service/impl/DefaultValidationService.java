/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.git.eca.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.git.eca.api.models.EclipseUser;
import org.eclipsefoundation.git.eca.config.MailValidationConfig;
import org.eclipsefoundation.git.eca.dto.CommitValidationStatus;
import org.eclipsefoundation.git.eca.helper.CommitHelper;
import org.eclipsefoundation.git.eca.helper.ProjectHelper;
import org.eclipsefoundation.git.eca.model.Commit;
import org.eclipsefoundation.git.eca.model.GitUser;
import org.eclipsefoundation.git.eca.model.ValidationRequest;
import org.eclipsefoundation.git.eca.model.ValidationResponse;
import org.eclipsefoundation.git.eca.namespace.APIStatusCode;
import org.eclipsefoundation.git.eca.service.UserService;
import org.eclipsefoundation.git.eca.service.ValidationService;
import org.eclipsefoundation.git.eca.service.ValidationStatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * Default service for validating external requests for ECA validation, as well as storing and retrieving information
 * about historic requests.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class DefaultValidationService implements ValidationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultValidationService.class);

    @Inject
    MailValidationConfig config;

    @Inject
    ProjectHelper projects;
    @Inject
    UserService users;
    @Inject
    ValidationStatusService statusService;


    @Override
    public ValidationResponse validateIncomingRequest(ValidationRequest req, RequestWrapper wrapper) {
        // get the projects associated with the current request, if any
        List<Project> filteredProjects = projects.retrieveProjectsForRequest(req);
        // build the response object that we can populate with messages (the message/error arrays are mutable unlike the rest of
        // the class)
        ValidationResponse r = ValidationResponse
                .builder()
                .setStrictMode(Boolean.TRUE.equals(req.getStrictMode()))
                .setTrackedProject(!filteredProjects.isEmpty())
                .setFingerprint(CommitHelper.generateRequestHash(req))
                .build();
        // check to make sure commits are valid
        if (req.getCommits().stream().anyMatch(c -> !CommitHelper.validateCommit(c))) {
            // for each invalid commit, add errors to output and return
            req
                    .getCommits()
                    .stream()
                    .filter(c -> !CommitHelper.validateCommit(c))
                    .forEach(c -> r
                            .addError(c.getHash(), "One or more commits were invalid. Please check the payload and try again",
                                    APIStatusCode.ERROR_DEFAULT));
            return r;
        }

        // get previous validation status messages
        List<CommitValidationStatus> statuses = statusService
                .getRequestCommitValidationStatus(wrapper, req, filteredProjects.isEmpty() ? null : filteredProjects.get(0).getProjectId());

        req.getCommits().stream().forEach(c -> {
            // get the current status if present
            Optional<CommitValidationStatus> status = statuses.stream().filter(s -> s.getCommitHash().equals(c.getHash())).findFirst();
            // skip the commit validation if already passed
            if (isValidationStatusCurrentAndValid(status, c)) {
                r.addMessage(c.getHash(), "Commit was previously validated, skipping processing", APIStatusCode.SUCCESS_SKIPPED);
                return;
            }
            // process the current commit
            processCommit(c, r, filteredProjects);
        });
        statusService.updateCommitValidationStatus(wrapper, r, req, statuses, filteredProjects.isEmpty() ? null : filteredProjects.get(0));
        return r;
    }

    /**
     * Process the current request, validating that the passed commit is valid. The author and committers Eclipse Account is
     * retrieved, which are then used to check if the current commit is valid for the current project.
     *
     * @param c the commit to process
     * @param response the response container
     * @param filteredProjects tracked projects for the current request
     */
    private void processCommit(Commit c, ValidationResponse response, List<Project> filteredProjects) {
        // retrieve the author + committer for the current request
        GitUser author = c.getAuthor();

        response.addMessage(c.getHash(), String.format("Reviewing commit: %1$s", c.getHash()));
        response.addMessage(c.getHash(), String.format("Authored by: %1$s <%2$s>", author.getName(), author.getMail()));

        // skip processing if a merge commit
        List<String> parents = c.getParents();
        if (parents != null && parents.size() > 1) {
            response
                    .addMessage(c.getHash(),
                            String.format("Commit '%1$s' has multiple parents, merge commit detected, passing", c.getHash()));
            return;
        }

        // retrieve the eclipse account for the author
        EclipseUser eclipseAuthor = getIdentifiedUser(author);
        // if the user is a bot, generate a stubbed user
        if (isAllowedUser(author.getMail()) || users.userIsABot(author.getMail(), filteredProjects)) {
            response
                    .addMessage(c.getHash(),
                            String.format("Automated user '%1$s' detected for author of commit %2$s", author.getMail(), c.getHash()));
            eclipseAuthor = EclipseUser.createBotStub(author);
        } else if (eclipseAuthor == null) {
            response
                    .addMessage(c.getHash(),
                            String
                                    .format("Could not find an Eclipse user with mail '%1$s' for author of commit %2$s", author.getMail(),
                                            c.getHash()));
            response.addError(c.getHash(), "Author must have an Eclipse Account", APIStatusCode.ERROR_AUTHOR_NOT_FOUND);
            return;
        }

        GitUser committer = c.getCommitter();
        // retrieve the eclipse account for the committer
        EclipseUser eclipseCommitter = getIdentifiedUser(committer);
        // check if whitelisted or bot
        if (isAllowedUser(committer.getMail()) || users.userIsABot(committer.getMail(), filteredProjects)) {
            response
                    .addMessage(c.getHash(),
                            String.format("Automated user '%1$s' detected for committer of commit %2$s", committer.getMail(), c.getHash()));
            eclipseCommitter = EclipseUser.createBotStub(committer);
        } else if (eclipseCommitter == null) {
            response
                    .addMessage(c.getHash(),
                            String
                                    .format("Could not find an Eclipse user with mail '%1$s' for committer of commit %2$s",
                                            committer.getMail(), c.getHash()));
            response.addError(c.getHash(), "Committing user must have an Eclipse Account", APIStatusCode.ERROR_COMMITTER_NOT_FOUND);
            return;
        }
        // validate author access to the current repo
        validateUserAccess(response, c, eclipseAuthor, filteredProjects, APIStatusCode.ERROR_AUTHOR);

        // check committer general access
        boolean isCommittingUserCommitter = isCommitter(response, eclipseCommitter, c.getHash(), filteredProjects);
        validateUserAccessPartial(response, c, eclipseCommitter, isCommittingUserCommitter, APIStatusCode.ERROR_COMMITTER);
    }

    /**
     * Validates author access for the current commit. If there are errors, they are recorded in the response for the
     * current request to be returned once all validation checks are completed.
     *
     * @param r the current response object for the request
     * @param c the commit that is being validated
     * @param eclipseUser the user to validate on a branch
     * @param filteredProjects tracked projects for the current request
     * @param errorCode the error code to display if the user does not have access
     */
    private void validateUserAccess(ValidationResponse r, Commit c, EclipseUser eclipseUser, List<Project> filteredProjects,
            APIStatusCode errorCode) {
        // call isCommitter inline and pass to partial call
        validateUserAccessPartial(r, c, eclipseUser, isCommitter(r, eclipseUser, c.getHash(), filteredProjects), errorCode);
    }

    /**
     * Allows for isCommitter to be called external to this method. This was extracted to ensure that isCommitter isn't
     * called twice for the same user when checking committer proxy push rules and committer general access.
     * 
     * @param r the current response object for the request
     * @param c the commit that is being validated
     * @param eclipseUser the user to validate on a branch
     * @param isCommitter the results of the isCommitter call from this class.
     * @param errorCode the error code to display if the user does not have access
     */
    private void validateUserAccessPartial(ValidationResponse r, Commit c, EclipseUser eclipseUser, boolean isCommitter,
            APIStatusCode errorCode) {
        String userType = "author";
        if (APIStatusCode.ERROR_COMMITTER.equals(errorCode)) {
            userType = "committer";
        }
        if (isCommitter) {
            r
                    .addMessage(c.getHash(),
                            String.format("Eclipse user '%s'(%s) is a committer on the project.", eclipseUser.getName(), userType));
        } else {
            r
                    .addMessage(c.getHash(),
                            String.format("Eclipse user '%s'(%s) is not a committer on the project.", eclipseUser.getName(), userType));
            // check if the author is signed off if not a committer
            if (eclipseUser.getECA().getSigned()) {
                r
                        .addMessage(c.getHash(),
                                String
                                        .format("Eclipse user '%s'(%s) has a current Eclipse Contributor Agreement (ECA) on file.",
                                                eclipseUser.getName(), userType));
            } else {
                r
                        .addMessage(c.getHash(), String
                                .format("Eclipse user '%s'(%s) does not have a current Eclipse Contributor Agreement (ECA) on file.\n"
                                        + "If there are multiple commits, please ensure that each author has a ECA.", eclipseUser.getName(),
                                        userType));
                r
                        .addError(c.getHash(),
                                String
                                        .format("An Eclipse Contributor Agreement is required for Eclipse user '%s'(%s).",
                                                eclipseUser.getName(), userType),
                                errorCode);
            }
        }
    }

    /**
     * Checks whether the given user is a committer on the project. If they are and the project is also a specification for
     * a working group, an additional access check is made against the user.
     *
     * <p>
     * Additionally, a check is made to see if the user is a registered bot user for the given project. If they match for
     * the given project, they are granted committer-like access to the repository.
     *
     * @param r the current response object for the request
     * @param user the user to validate on a branch
     * @param hash the hash of the commit that is being validated
     * @param filteredProjects tracked projects for the current request
     * @return true if user is considered a committer, false otherwise.
     */
    private boolean isCommitter(ValidationResponse r, EclipseUser user, String hash, List<Project> filteredProjects) {
        // iterate over filtered projects
        for (Project p : filteredProjects) {
            LOGGER.debug("Checking project '{}' for user '{}'", p.getName(), user.getName());
            // check if any of the committers usernames match the current user
            if (p.getCommitters().stream().anyMatch(u -> u.getUsername().equals(user.getName()))) {
                // check if the current project is a committer project, and if the user can
                // commit to specs
                if (p.getSpecWorkingGroup().isPresent() && !user.getECA().getCanContributeSpecProject()) {
                    // set error + update response status
                    r
                            .addError(hash, String
                                    .format("Project is a specification for the working group '%1$s', but user does not have permission to modify a specification project",
                                            p.getSpecWorkingGroup()),
                                    APIStatusCode.ERROR_SPEC_PROJECT);
                    return false;
                } else {
                    LOGGER.debug("User '{}' was found to be a committer on current project repo '{}'", user.getMail(), p.getName());
                    return true;
                }
            }
        }
        // check if user is a bot, either through early detection or through on-demand
        // check
        if ((user.getIsBot() != null && Boolean.TRUE.equals(user.getIsBot())) || users.userIsABot(user.getMail(), filteredProjects)) {
            LOGGER.debug("User '{} <{}>' was found to be a bot", user.getName(), user.getMail());
            return true;
        }
        return false;
    }

    /**
     * Checks against internal allow list for global bypass users, like webmaster and dependabot.
     * 
     * @param mail the mail address to check for allow list
     * @return true if user email is in allow list, false otherwise
     */
    private boolean isAllowedUser(String mail) {
        return StringUtils.isNotBlank(mail) && config.allowList().indexOf(mail) != -1;
    }

    /**
     * Retrieves an Eclipse Account user object given the Git users email address (at minimum). This is facilitated using
     * the Eclipse Foundation accounts API, along short lived in-memory caching for performance and some protection against
     * duplicate requests.
     *
     * @param user the user to retrieve Eclipse Account information for
     * @return the Eclipse Account user information if found, or null if there was an error or no user exists.
     */
    private EclipseUser getIdentifiedUser(GitUser user) {
        // check if the external ID is set, and if so, attempt to look the user up.
        if (StringUtils.isNotBlank(user.getExternalId())) {
            // right now this is only supported for Github account lookups, so that will be used
            EclipseUser actualUser = users.getUserByGithubUsername(user.getExternalId());
            // if present, return the user. Otherwise, log and continue processing
            if (actualUser != null) {
                return actualUser;
            } else {
                LOGGER
                        .debug("An external ID of {} was passed, but no matching Eclipse users found. Falling back on default email lookup.",
                                user.getExternalId());
            }
        }

        // don't process an empty email as it will never have a match
        if (StringUtils.isBlank(user.getMail())) {
            LOGGER.debug("Cannot get identified user if user is empty, returning null");
            return null;
        }
        // get the Eclipse account for the user
        try {
            // use cache to avoid asking for the same user repeatedly on repeated requests
            EclipseUser foundUser = users.getUser(user.getMail());
            if (foundUser == null) {
                LOGGER.warn("No users found for mail '{}'", user.getMail());
            }
            return foundUser;
        } catch (WebApplicationException e) {
            Response r = e.getResponse();
            if (r != null && r.getStatus() == Status.NOT_FOUND.getStatusCode()) {
                LOGGER.error("No users found for mail '{}'", user.getMail());
            } else {
                LOGGER.error("Error while checking for user", e);
            }
        }
        return null;
    }

    /**
     * <p>
     * Checks the following to determine whether a commit has already been validated:
     * </p>
     * <ul>
     * <li>The validation status exists
     * <li>The validation status has a user mail associated with it
     * <li>Validation status has no present errors
     * <li>Modification date is either unset or matches when set
     * <li>User mail is set and matches (ignores case)
     * </ul>
     * 
     * If any of these checks fail, then the commit should be revalidated.
     * 
     * @param status the current commits validation status if it exists
     * @param c the commit that is being validated
     * @return true if the commit does not need to be (re)validated, false otherwise.
     */
    private boolean isValidationStatusCurrentAndValid(Optional<CommitValidationStatus> status, Commit c) {
        return status.isPresent() && status.get().getErrors().isEmpty() && c.getAuthor() != null
                && StringUtils.isNotBlank(status.get().getUserMail())
                && status.get().getUserMail().equalsIgnoreCase(c.getAuthor().getMail())
                && (c.getLastModificationDate() == null || status.get().getLastModified().equals(c.getLastModificationDate()));
    }
}
