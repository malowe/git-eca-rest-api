#!/usr/bin/env ruby
# encoding: UTF-8

require 'json'
require 'httparty'
require 'multi_json'
WIKI_REGEX_MATCH = /.*\.wiki$/
HOST_URL='https://gitlab.eclipse.org'
API_URL='https://api.eclipse.org'
# this should be removed as soon as Oniro is onside again
ALLOW_LIST_PROJECTS = ['/eclipse/oniro-core/meta-ts',
'/eclipse/oniro-core/meta-ledge-sesure',
'/eclipse/oniro-core/linux',
'/eclipse/oniro-core/linux-meta',
'/eclipse/oniro-core/llvm-project',
'/eclipse/oniro-core/meta-arm',
'/eclipse/oniro-core/meta-av96',
'/eclipse/oniro-core/meta-binaryaudit',
'/eclipse/oniro-core/meta-clang',
'/eclipse/oniro-core/meta-openembedded',
'/eclipse/oniro-core/meta-riscv',
'/eclipse/oniro-core/meta-seco-intel',
'/eclipse/oniro-core/meta-zephyr'
]
STRICT_MODE_ROOT_GROUPS = ['eclipse-research-labs/']

## Track whether forked project for error reporting (failing vs non-failing)
is_forked_project = false

## Process the commit into a hash object that will be posted to the ECA validation service
def process_commit(sha)
  commit_parents_raw = `git show -s --format='%P' #{sha}`
  commit_parents = commit_parents_raw.split(/\s/)
  return {
    :author => {
      :name => `git show -s --format='%an' #{sha}`.force_encoding("utf-8"),
      :mail => `git show -s --format='%ae' #{sha}`.force_encoding("utf-8"),
    },
    :committer => {
      :name => `git show -s --format='%cn' #{sha}`.force_encoding("utf-8"),
      :mail => `git show -s --format='%ce' #{sha}`.force_encoding("utf-8"),
    },
    :body => `git show -s --format='%B' #{sha}`.force_encoding("utf-8"),
    :subject => `git show -s --format='%s' #{sha}`.force_encoding("utf-8"),
    :hash => `git show -s --format='%H' #{sha}`,
    :parents => commit_parents
  }
end

## Take in a project namespace, and see if it starts with any of the targeted root groups.
def use_strict_mode(project_namespace)
  return STRICT_MODE_ROOT_GROUPS.any? { |root_group| project_namespace.start_with?(root_group) }
end

def nil_or_empty(o)
  return o.nil? || o.empty?
end

## read in the access token from secret file
if (!File.file?("/etc/gitlab/eca-access-token"))
  puts "GL-HOOK-ERR: Internal server error, please contact administrator. Error, secret not found" 
  exit 1
end
access_token_file = File.open("/etc/gitlab/eca-access-token")
access_token = access_token_file.read.chomp
access_token_file.close
if (access_token.empty?)
  puts "GL-HOOK-ERR: Internal server error, please contact administrator. Error, secret not found" 
  exit 1
end

## Read in the arguments passed from GitLab and split them to an arg array
stdin_raw = ARGF.read;
stdin_args = stdin_raw.split(/\s+/)

## Set the vars for the commit hashes of current pre-receive event
previous_head_commit = stdin_args[0]
new_head_commit = stdin_args[1]

gl_repo = ENV['GL_REPOSITORY']
if (nil_or_empty(gl_repo)) then 
  puts "No Gitlab repository set, likely dealing with non-repo commit, skipping"
  exit 0
elsif (gl_repo =~ /^wiki-/ || gl_repo =~ /^group-\d+-wiki$/) then
  puts "Commit is associated with a wiki, and does not need to be validated. Skipping."
  exit 0
elsif (gl_repo !~ /^project-/) then
  puts "GL_REPOSITORY envvar is improperly set does not match expected format (#{gl_repo}), cannot validate"
  exit 1
end
## Get the project ID from env var, extracting from pattern 'project-###'
project_id = gl_repo[8..-1]

## Get data about project from API
project_response = HTTParty.get("#{HOST_URL}/api/v4/projects/#{project_id}", 
  :headers => {
    'Authorization' => 'Bearer ' + access_token
  })
## Format data to be able to easily read and process it
project_json_data = MultiJson.load(project_response.body)
if (nil_or_empty(project_json_data) || project_json_data.class.name == 'Array') then
  puts "Couldn't load project data, assumed non-tracked project and skipping validation."
  exit 0
end
## Get the web URL, checking if project is a fork to get original project URL
if (!nil_or_empty(project_json_data['forked_from_project'])) then
  puts "Fork detected, using forked from project's URL for better commit validation."
  project_url = project_json_data['forked_from_project']['web_url']
  is_forked_project = true
else 
  project_url = project_json_data['web_url']
end

## This can happen for group wikis by inference from some production-only issues
if (nil_or_empty(project_url)) then
  puts "Could not determine a web URL for project, likely not a fully-qualified project, skipping"
  exit 0
elsif (ALLOW_LIST_PROJECTS.any? {|repo_namespace| "#{HOST_URL}#{repo_namespace}" == project_url}) then
  puts "Found allow listed project, skipping validation"
  exit 0
end

## Get all new commits for branch, relative to itself for existing branch, relative to tree for new
diff_git_commits_raw = ''
if (previous_head_commit =~ /\0+/) then
  ## This isn't perfect as its relative to fork, but should be huge improvement
  diff_git_commits_raw = `git rev-list #{new_head_commit}  --not --branches=*`
else
  diff_git_commits_raw = `git rev-list #{previous_head_commit}...#{new_head_commit}`
end
diff_git_commits = diff_git_commits_raw.split(/\n/)

## Check if the are commits to validate. If there are none we can end processing here
if (diff_git_commits.empty?) then
  puts "There are no commits to validate for current push, skipping validation step"
  exit 0
end

processed_git_data = []
diff_git_commits.each do |commit|
  processed_git_data.push(process_commit(commit))
end

is_strict_enforced = use_strict_mode(project_json_data['path_with_namespace'])
## Create the JSON payload
json_data = {
  :repoUrl => project_url,
  :provider => 'gitlab',
  :commits => processed_git_data,
  :strictMode => is_strict_enforced
}
## Generate request
response = HTTParty.post("#{API_URL}/git/eca", :body => MultiJson.dump(json_data), 
  :headers => { 
    'Content-Type' => 'application/json',
    'charset' => 'utf-8'
  })
## convert request to hash map
parsed_response = Hash.new
begin
  parsed_response = MultiJson.load(response.body)
rescue MultiJson::ParseError
  puts "GL-HOOK-ERR: Unable to validate commit, server error encountered.\n\nPlease contact the administrator, and retry the commit at a later time.\n\n"
  exit 1
else
  ## Tracks if warnings/errors were issued for request 
  contained_warnings_errors = false
  ## for each discovered hash commit tracked by response, report if it was OK
  commit_keys = parsed_response['commits'].keys
  commit_keys.each do |key|
    commit_status = parsed_response['commits'][key]

    ## Write the commit header with symbol indicating full or mixed success, or error state
    if (commit_status['errors'].empty? && commit_status['warnings'].empty?) then
      puts "Commit: #{key}\t\t✔\n\n"
    elsif (commit_status['errors'].empty?) then
      puts "Commit: #{key}\t\t~\n\n"
    else
      puts "Commit: #{key}\t\tX\n\n"
    end

    ## put all of the normal messages for the commit
    commit_status['messages'].each do |msg|
      puts "\t#{msg['message']}"
    end

    ## write warnings if they exist
    if (!commit_status['warnings'].empty?) then
      contained_warnings_errors = true
      puts "\nWarnings:"
      commit_status['warnings'].each do |msg|
        puts "\t#{msg['message']}"
      end
    end

    ## write errors if they exist
    if (!commit_status['errors'].empty?) then
      contained_warnings_errors = true
      puts "\nErrors:"
      commit_status['errors'].each do |error|
        puts "#{error['message']}"
      end
    end

    ## At end, print help message for warnings/errors
    if (!commit_status['warnings'].empty? || !commit_status['errors'].empty?) then
      puts "Any warnings or errors noted above may indicate compliance issues with committer ECA requirements. More information may be found on https://www.eclipse.org/legal/ECA.php"
    end
    puts "\n\n"
  end
  if (contained_warnings_errors) then
    puts "GL-HOOK-ERR: More information is available at https://api.eclipse.org/git/eca/status/#{parsed_response['fingerprint']}/ui"
  else
    puts "More information is available at https://api.eclipse.org/git/eca/status/#{parsed_response['fingerprint']}/ui"
  end
  ## after parsing 
  if (!parsed_response.nil? && parsed_response['trackedProject'] == false && !is_strict_enforced) then
    if (contained_warnings_errors) then
      puts "Errors or warnings were encountered while validating sign-off in current request for non-project repository.\n\nValidation is currently not required for non-project repositories, continuing."
    end
    exit 0
  end
end
## If error, exit as status 1
if (response.code == 403 && is_forked_project && !is_strict_enforced) then
  puts "Errors detected, but commits will be allowed for forked repository. Commit errors reported in this push will block merge into origin repository until resolved."
elsif (response.code == 403) then
  exit 1
end
